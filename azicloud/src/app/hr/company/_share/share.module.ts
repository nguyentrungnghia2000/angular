import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { HeadingComponent } from "../../company/_share/heading/heading.component";
import { LeftMenuComponent } from "./left-menu/left-menu.component";
import {BreadcrumbModule} from 'primeng/breadcrumb';
import { CommonModule } from '@angular/common';

//ng-zorro
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { AppSharedModule } from "src/app/_share/appShare.module";

@NgModule({
    declarations:[
      LeftMenuComponent,
      HeadingComponent
    ],
    imports:[
        RouterModule,
        BreadcrumbModule,
        NzPaginationModule,
        NzSelectModule,
        CommonModule,
        AppSharedModule
    ],
    exports:[
      LeftMenuComponent,
      HeadingComponent
    ]

})

export class ShareModule {}
