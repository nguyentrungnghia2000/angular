import { NgModule } from "@angular/core";
import { CvRoutingModule } from "./cv-routing.module";
import { CvComponent } from "./cv.component";
import { ShareModule } from "./_share/share.module";

@NgModule({
    declarations:[
      CvComponent
    ],
    imports:[
        ShareModule,
        CvRoutingModule
    ],
    exports:[]
})

export class CvModule {}
