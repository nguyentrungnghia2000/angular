export class CardID{
  id!: number;
  identity_card!: number;
  identity_card_date!: string;
  identity_card_issued!: string;
  identity_front_image_id!: number;
  identity_front_image_filepath!: string;
  identity_front_image_filename!: string;
  identity_backside_image_id!: number;
  identity_backside_image_filepath!: string;
  identity_backside_image_filename!: string;
}
