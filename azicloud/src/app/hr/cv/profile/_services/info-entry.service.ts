import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, map } from 'rxjs/operators';
import { HttpService } from 'src/app/_share/services/http.service';
import { API } from 'src/app/_share/api/api.hr.config';

@Injectable({
  providedIn: 'root'
})
export class InfoEntryService {
  token: string ='';
  boundary: any;

  constructor(private http: HttpService) {
    this.token = localStorage.getItem('access_token');
   }

  getEmployeeInfo(id){
    return this.http.sendToServer('GET', API.EMPLOYEE.PERSONAL_VIEW(id), null, {'Authorization': `Bearer ${this.token}`});
  }

  updateEmployeeInfo(id, name: string, phone: string, email: string, tax_code: string,
                          birth_day: number, birth_month: number, birth_year: number, nationality_id: number,
                          religion_name: string, nation_key: string, gender_key: string, marital_status_key: string,
                          active: boolean, religion: boolean){
    return this.http.sendToServer('POST', API.EMPLOYEE.PERSONAL_UPDATE(id), {
      name,
      phone,
      email,
      tax_code,
      birth_day,
      birth_month,
      birth_year,
      nationality_id,
      religion_name,
      nation_key,
      gender_key,
      marital_status_key,
      active,
      religion
    }, {'Authorization': `Bearer ${this.token}`});
  }

  getListEmployeeHealth(id){
    return this.http.sendToServer('GET', API.EMPLOYEE.HEALTH_LIST_VIEW(id), null, {'Authorization': `Bearer ${this.token}`});
  }

  createEmployeeHealth(_id, id: number, height: number, weight: number, blood_key: string,
                            status: string, check_date: string, congenital_diseases: string, note: string,){
    return this.http.sendToServer('POST', API.EMPLOYEE.HEALTH_CREATE(_id), {
      id,
      height,
      weight,
      blood_key,
      status,
      check_date,
      congenital_diseases,
      note
    }, {'Authorization': `Bearer ${this.token}`});
  }

  deleteEmployeeHealth(id){
    return this.http.sendToServer('POST', API.EMPLOYEE.HEALTH_DELETE(id), null, {'Authorization': `Bearer ${this.token}`});
  }

  getEmployeeHealth(id){
    return this.http.sendToServer('GET', API.EMPLOYEE.HEALTH_VIEW(id), null, {'Authorization': `Bearer ${this.token}`});
  }

  getEmployeeBank(id){
    return this.http.sendToServer('GET', API.EMPLOYEE.BANK_VIEW(id), null, {'Authorization': `Bearer ${this.token}`});
  }

  updateEmployeeBank(id, bank_id: number, bank_account_name: string, bank_account_number: string, bank_branch: string){
    return this.http.sendToServer('POST', API.EMPLOYEE.BANK_UPDATE(id), {
      bank_id,
      bank_account_name,
      bank_account_number,
      bank_branch
    }, {'Authorization': `Bearer ${this.token}`});
  }

  getEmployeeDescription(id){
    return this.http.sendToServer('GET', API.EMPLOYEE.DESCRIPTION_VIEW(id), null, {'Authorization': `Bearer ${this.token}`});
  }

  updateEmployeeDescription(id, description_infor: string){
    return this.http.sendToServer('POST', API.EMPLOYEE.DESCRIPTION_UPDATE(id), {
      description_infor
    }, {'Authorization': `Bearer ${this.token}`});
  }

  getEmployeeCardID(id){
    return this.http.sendToServer('GET', API.EMPLOYEE.CARD_ID_VIEW(id), null, {'Authorization': `Bearer ${this.token}`});
  }

  updateEmployeeCardID(id, identity_card: number, identity_card_date: string,
                            identity_card_issued: string, identity_front_image_id: number, identity_backside_image_id: number){
    return this.http.sendToServer('POST', API.EMPLOYEE.CARD_ID_UPDATE(id), {
      identity_card,
      identity_card_date,
      identity_card_issued,
      identity_front_image_id,
      identity_backside_image_id
    }, {'Authorization': `Bearer ${this.token}`});
  }

  getEmployeePassport(id){
    return this.http.sendToServer('GET', API.EMPLOYEE.PASSPORT_VIEW(id), null, {'Authorization': `Bearer ${this.token}`});
  }

  updateEmployeePassport(id, passport: string, passport_start_date: string, passport_end_date: string,
                              passport_issued: string, passport_front_image_id: number, passport_backside_image_id: number){
    return this.http.sendToServer('POST', API.EMPLOYEE.PASSPORT_UPDATE(id), {
      passport,
      passport_start_date,
      passport_end_date,
      passport_issued,
      passport_front_image_id,
      passport_backside_image_id
    }, {'Authorization': `Bearer ${this.token}`});
  }

  getListCountries(){
    return this.http.sendToServer('GET', API.EMPLOYEE.COUNTRIES_LIST(' '), null, {'Authorization': `Bearer ${this.token}`});
  }

  getListGenders(){
    return this.http.sendToServer('GET', API.EMPLOYEE.GENDER_NATION_MARITAL_BLOOD_LIST('gender.type'), null, {'Authorization': `Bearer ${this.token}`});
  }

  getListNations(){
    return this.http.sendToServer('GET', API.EMPLOYEE.GENDER_NATION_MARITAL_BLOOD_LIST('nation.type'), null, {'Authorization': `Bearer ${this.token}`});
  }

  getListMaritals(){
    return this.http.sendToServer('GET', API.EMPLOYEE.GENDER_NATION_MARITAL_BLOOD_LIST('marital.status'), null, {'Authorization': `Bearer ${this.token}`});
  }

  getListBloods(){
    return this.http.sendToServer('GET', API.EMPLOYEE.GENDER_NATION_MARITAL_BLOOD_LIST('blood.type'), null, {'Authorization': `Bearer ${this.token}`});
  }

  getListBanks(id){
    return this.http.sendToServer('GET', API.EMPLOYEE.BANK_LIST(' ', id), null, {'Authorization': `Bearer ${this.token}`});
  }

  uploadImage(id:number, company_code:string, file: any){
    const formData = new FormData();
    formData.append("file", file, file.name);
    return this.http.sendToServer('POST', API.EMPLOYEE.UPLOAD_IMAGE(id, company_code, 'provider.local'), formData,{'Authorization': `Bearer ${this.token}`});
  }
}
