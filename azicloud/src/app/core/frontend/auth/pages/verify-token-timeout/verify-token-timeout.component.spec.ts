import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyTokenTimeoutComponent } from './verify-token-timeout.component';

describe('VerifyTokenTimeoutComponent', () => {
  let component: VerifyTokenTimeoutComponent;
  let fixture: ComponentFixture<VerifyTokenTimeoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerifyTokenTimeoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyTokenTimeoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
