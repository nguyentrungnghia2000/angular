export const API ={
    EMPLOYEE: {
        PERSONAL_VIEW:(id) =>`/hr/employee/${id}/personal/view` ,
        PERSONAL_UPDATE:(id) =>`/hr/employee/${id}/personal/update`,
        BANK_VIEW:(id) =>`/hr/employee/${id}/bank/view`,
        BANK_UPDATE:(id) =>`/hr/employee/${id}/bank/update`,
        DESCRIPTION_VIEW:(id) =>`/hr/employee/${id}/description/view`,
        DESCRIPTION_UPDATE:(id) =>`/hr/employee/${id}/description/update`,
        ADDRESS_VIEW: (id) =>`/hr/employee/${id}/address/view`,
        ADDRESS_UPDATE:(id) =>`/hr/employee/${id}/address/update`,
        EMERGENCY_VIEW:(id) =>`/hr/employee/${id}/emergency/view`,
        EMERGENCY_UPDATE:(id) =>`/hr/employee/${id}/emergency/update`,
        PERMANENT_ADDRESS_VIEW:(id) =>`/hr/employee/${id}/permanent-address/view`,
        PERMANENT_ADDRESS_UPDATE:(id) =>`/hr/employee/${id}/permanent-address/update`,
        PASSPORT_VIEW:(id) =>`/hr/employee/${id}/passport/view`,
        PASSPORT_UPDATE:(id) =>`/hr/employee/${id}/passport/update`,
        IDENTITY_VIEW:(id) =>`/hr/employee/${id}/identity/view`,
        IDENTITY_UPDATE:(id) =>`/hr/employee/${id}/identity/update`,
        TAX_CODE_VIEW:(id) =>`/hr/employee/${id}/tax-code/view`,
        TAX_CODE_UPDATE:(id) =>`/hr/employee/${id}/tax-code/update`,
        IMAGE_UPDATE:(id) =>`/hr/employee/${id}/image/update`,
        DEPENDENT_LIST:(id) => `/hr/employee/${id}/dependent/list`,
        DEPENDENT_CREATE:(id) => `/hr/employee/${id}/dependent/create`,
        DEPENDENT_DELETE:(id) => `/hr/employee/dependent/${id}/delete`,
        DEPENDENT_VIEW:(id) => `/hr/employee/dependent/${id}/view`,
        COUNTRY_LIST:`/res/country/list`,
        AREA_LIST:(id) => `/res/country/${id}/area/list`,
        TERRITORY_LIST:(id) => `/res/area/${id}/territory/list`,
        SUBTERRITORY_LIST:(id) => `/res/territory/${id}/subterritory/list`,
        HEALTH_VIEW:(id) => `/hr/employee/employee-health/${id}/view`,
        HEALTH_LIST_VIEW:(id) => `/hr/employee/${id}/employee-health/list`,
        HEALTH_CREATE:(id) => `/hr/employee/${id}/employee-health/create`,
        HEALTH_DELETE:(id) => `/hr/employee/employee-health/${id}/delete`,
        GENDER_NATION_MARITAL_BLOOD_LIST:(parent_key) => `/res/config/list?parent_key=${parent_key}`,
        COUNTRIES_LIST:(search_text) => `/res/country/list?search_text=${search_text}`,
        BANK_LIST:(search_text: string, id: number) => `/res/country/${id}/bank/list?search_text=${search_text}`,
        CARD_ID_VIEW:(id) => `/hr/employee/${id}/identity/view`,
        CARD_ID_UPDATE:(id) => `/hr/employee/${id}/identity/update`,
        SKILL_LIST:(id) => `/hr/employee/${id}/skill/list`,
        WEB_PERSONAL_VIEW:(id) => `/hr/employee/${id}/web/persional/view`,
        UPLOAD_IMAGE:(id: number, company_code: string, type: string) => `/upload/company/${id}/image?company_code=${company_code}&type=${type}`,​
        GENDER_NATION_MARITAL_LIST:(parent_key) => `/res/config/list?parent_key=${parent_key}`,



        EMPLOYEESKILL:{
            SKILL_LIST:(id) => `/hr/employee/${id}/skill/list`,
            CREATE:(id) => `/hr/employee/${id}/skill/create`,
            DELETE:(id) => `/hr/employee/skill/${id}/delete`,
            VIEW:(skill_id) => `/hr/employee/skill/${skill_id}/view`
        },


        RESUMELINE:{
            LIST:(id) => `/hr/employee/${id}/resume-line/list`,
            CREATE: (id) => `/hr/employee/${id}/resume-line/create`,
            DELETE:(id) => `/hr/employee/resume-line/${id}/delete`,
            VIEW:(id) => `/hr/employee/resume-line/${id}/view`

        },
      },



    COMPANY:{
            SKILL:{
                CREATE:'/company/skill/create',
                LIST:(id) =>`/company/${id}/skill/list`,
                DELETE:(id) => `/company/skill//${id}/delete`,
                VIEW:(id) => `/company/skill/${id}/view`,
                SKILL_TYPE_SKILL_LIST:(id) => `/company/skill-type/${id}/skill/list`
            },
            SKILL_TYPE:{
                List:(id) =>`/company/${id}/skill-type/list`
            },
            SKILL_LEVEL:{
                List:(id) => `/company/skill/${id}/skill-level/list`,
                VIEW:(id) => `/company/skill-level/${id}/view`
            },

            RESUMELINETYPE:{
                List:(id) => `/company/${id}/resume-line/list`
            },

            EMPLOYEE:{
              LIST:(search_text) => `/company/employee/list?search_text=${search_text}`,
              LIST_WITHOUT_TEXT: '/company/employee/list',
            }


    },



}
