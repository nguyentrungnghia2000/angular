export class Current{
  current_address: string;
  current_area_id: number;
  current_area_name: string;
  current_country_id: number;
  current_country_name:string;
  current_subterritory_id: number;
  current_subterritory_name: string;
  current_territory_id: number;
  current_territory_name: string;
  id: number;
  mobile: string;
  phone: string;
}
export class Emergence{
  id: number;
  emergency_address: string;
  name: string;
  emergency_relationship: string;
  emergency_mobile: string;
  emergency_phone: string;
}
export class Permanent{
  id:number;
  permanent_country_name: string;
  permanent_area_name:string;
  permanent_territory_name: string;
  permanent_subterritory_name: string;
  permanent_address: string;
  domiciled: string;
  permanent_country_id: number;
  permanent_area_id: number;
  permanent_territory_id: number;
  permanent_subterritory_id: number;
  current_address: string;
  phone:number;
  mobile:number;
}
export class Dependent{
  id: number;
  employee_id: number;
  country_id: number;
  country_name: string;
  area_id: number;
  area_name: string;
  territory_id: number;
  territory_name: string;
  subterritory_id: number;
  subterritory_name: string;
  name: string;
  email: string;
  relationship_key: string;
  relationship_name: string;
  birth_day: number;
  birth_month: number;
  birth_year: number;
  gender_key: string;
  gender_name: SVGStringList;
  identity_card: string;
  tax_code: string;
  start_date: string;
  end_date: string;
}
export class Dependent_list{
  id: number;
  name: string;
  relationship_key:string;
  relationship_name:string;
  tax_code: string;
}
