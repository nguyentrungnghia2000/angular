import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { InfoEntryService } from '../../../_services/info-entry.service';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

  description: string = '';
  display: boolean = false;
  temp = "";
  userId!: any;
  uid: any;

  loading$ = this.loaderService.isLoading$;

  constructor(private infoEntryService: InfoEntryService,
              private toastr: ToastrService,
              public loaderService: LoaderService,
              private renderer: Renderer2,
              @Inject(DOCUMENT) private doc: Document) {
                this.userId = localStorage.getItem('user');
                this.uid = parseInt(this.userId);
              }

  ngOnInit(): void {
    this.loadDescription();
  }

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  showDialog() {
    this.temp = this.description;
    this.display = true;
    this.updateBodyPadding();
  }

  closeDialog(){
    this.bringFromEditor(this.description);
    this.bringFromEditor(this.temp);
    this.description = this.temp;
    this.temp = "";
    this.display = false;
    this.loadDescription();
    this.updateBodyPadding(true);
  }

  loadDescription(){

    this.infoEntryService.getEmployeeDescription(this.uid).subscribe((res: any) =>{
      if(res !== null){
        this.description = res.description_infor;
        // this.description = this.bringToEditor(this.description);
      }
      else{
        this.toastr.error('Không thể tải về thông tin giới thiệu', 'LỖI');
      }
      });
  }

  updateDescription(){
    this.description = this.bringFromEditor(this.description);
    this.infoEntryService.updateEmployeeDescription(this.uid, this.description).subscribe((res: any)=>{
      if(res.result === 1 ){
        this.temp = "";
        this.toastr.success('Cập nhật mô tả thành công', 'THÔNG BÁO');
        this.closeDialog();
      }
      else {
        this.toastr.error('Cập nhật mô tả thất bại', 'LỖI');
      }
    })
  }

  bringToEditor(str: string){
    str = `<h3> ${str} </h3>`;
    return str;
  }

  //convert text from the editor back to normal string
  bringFromEditor(str: string){
    str = str.replace('<p>', '').replace('</p>', '');
    return str;
  }

}
