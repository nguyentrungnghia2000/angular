import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//components
import { DialogOneColComponent } from './component/dialog-one-col/dialog-one-col.component';
import { DialogTwoColComponent } from './component/dialog-two-col/dialog-two-col.component';
import { DropdownComponent } from './component/dropdown/dropdown.component';

//primeNG
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import { LoadingComponent } from './component/loading/loading.component';

@NgModule({
  declarations: [
    DialogOneColComponent,
    DialogTwoColComponent,
    DropdownComponent,
    LoadingComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    //primeNG
    DropdownModule,
    DialogModule
 
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,

    //component
    DialogOneColComponent,
    DialogTwoColComponent,
    DropdownComponent,
    LoadingComponent,

    //primeNG
    DropdownModule,
    DialogModule

  ],
  providers: []
})
export class AppSharedModule { }
