import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import firebase from 'firebase/app';
import 'firebase/auth';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { AuthService } from '../../_services/auth.service';
import { LoginService } from '../../_services/login.service';
import { WindowService } from '../../_services/window.service';

@Component({
  selector: 'app-opt',
  templateUrl: './opt.component.html',
  styleUrls: ['./opt.component.scss']
})


export class OptComponent implements OnInit {

  code = new codeNumber();

  windowRef: any;
  user: any;
  user_name = '';
  token_otp = '';

  constructor(private router: Router,
    private win: WindowService,
    private auth: AuthService,
    private loginService: LoginService,
    private toastr: ToastrService) {
      this.code = new codeNumber();
      this.user_name = localStorage.getItem('user_name');
      console.log(this.user_name);
    }

  ngOnInit(): void {
  }

  sendCode() {
    const appVerifier = this.windowRef.recaptchaVerifier;
    const userInfo = localStorage.getItem('user_Info')
    const pNumber = JSON.parse(userInfo);
    firebase.auth().signInWithPhoneNumber(pNumber.phoneNumber, appVerifier)
      .then((confirmationResult) => {

        this.windowRef.confirmationResult = confirmationResult;
        // ...
      }).catch((error) => {
        console.log(error)
      });
  }

  verifyCode() {
    this.token_otp = this.code.getCode();
    this.loginService.availableOTP(this.user_name, this.token_otp).subscribe((res: any) => {
      if(res.result === 1){
        this.toastr.success('Xác thực OTP thành công', 'THÔNG BÁO');
        localStorage.removeItem('user_name');
        this.router.navigate(['/core/frontend/auth/regisworkplace']);
      } else if(res.result === -1){
        this.toastr.error('Tài khoảng không tồn tại', 'LỖI');
      } else if(res.result === -2){
        this.toastr.warning('OTP không đúng. Xin kiểm tra lại', 'CẢNH BÁO');
      } else if(res.result === -3){
        this.toastr.error('OTP hết hạn', 'LỖI');
      } else if(res.result === -4){
        this.toastr.error('Tài khoản người dùng đã bị khóa', 'LỖI');
      }
    });

  }

  onKeyUp(event: any, pre: any, cur: any, next: any){
    let length = cur.value.length;
    let maxLength = cur.getAttribute('maxlength');
    if(length == maxLength){
      if(next != ""){
        next.focus();
      }
    }
    if(event.key === "Backspace"){
      if(pre != ""){
        pre.focus();
      }
    }
  }

  resendOTP(){
    let user_name = localStorage.getItem('user_name');
    this.loginService.renewOTP(user_name, 'string').subscribe((res: any) => {
      if(res.result === 1){
        this.toastr.success('Đã gửi lại mã OTP', 'THÔNG BÁO');
      }
    });
  }
}

export class codeNumber {
  first: string = '';
  second: string = '';
  third: string = '';
  fourth: string = '';
  fifth: string = '';
  sixth: string = '';

  public getCode() {
    const num = this.first + this.second + this.third + this.fourth + this.fifth + this.sixth;
    return num;
  }
}
