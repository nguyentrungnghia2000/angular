export class Company{
  id: number;
  name: string;
  address: string;
  url: string;
  tax_code: string;
  fileimage: string;
}
