import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CompanyComponent } from "./company.component";


const routes: Routes = [
    { path:'', component: CompanyComponent, children: [
            { path:'employee', loadChildren:() => import('./employee/employee.module').then(m => m.EmployeeModule) },
            { path:'', pathMatch:'full',redirectTo:'employee' }
        ]
    }
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class CompanyRoutingModule {}
