import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/_share/services/loader.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  loading$ = this.loaderService.isLoading$;

  constructor(public loaderService: LoaderService) { }

  ngOnInit(): void {
  }

}
