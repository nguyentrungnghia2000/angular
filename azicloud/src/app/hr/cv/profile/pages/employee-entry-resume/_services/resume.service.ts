import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { shareReplay } from "rxjs/operators";
import { API } from "src/app/_share/api/api.hr.config";

import { HttpService } from "src/app/_share/services/http.service";


@Injectable()
export class ResumeService{

    constructor(private http:HttpService){}

    token = localStorage.getItem('access_token');

  getSkillList(id){
      return this.http.sendToServer('GET',API.COMPANY.SKILL.LIST(id),null,{'Authorization': `Bearer ${this.token}`}).pipe(shareReplay());
  }

  getSkillType(id){
      return this.http.sendToServer('GET',API.COMPANY.SKILL_TYPE.List(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  getSkillLevel(id){
      return this.http.sendToServer('GET',API.COMPANY.SKILL_LEVEL.List(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  viewSkillLevel(id){
      return this.http.sendToServer('GET',API.COMPANY.SKILL_LEVEL.VIEW(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  getEmployeeSkillList(id){
      return this.http.sendToServer('GET',API.EMPLOYEE.EMPLOYEESKILL.SKILL_LIST(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  getExperience(id){
      return this.http.sendToServer('GET',API.EMPLOYEE.RESUMELINE.LIST(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  getResumeline(id){
    return this.http.sendToServer('GET',API.EMPLOYEE.RESUMELINE.VIEW(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  getResumeLineType(id){
      return this.http.sendToServer('GET',API.COMPANY.RESUMELINETYPE.List(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  createSkill(idEmp:number,id:number,skill_id:number,skill_type_id:number,skill_level_id:number){
      return this.http.sendToServer('POST',API.EMPLOYEE.EMPLOYEESKILL.CREATE(idEmp),{id,skill_id,skill_type_id,skill_level_id},{'Authorization': `Bearer ${this.token}`});
  }

  createResumeLine(idEmp:number,id:number,type_id:number,name:string,start_date:string,end_date:string,description:string){
      return this.http.sendToServer('POST',API.EMPLOYEE.RESUMELINE.CREATE(idEmp),{
          id,type_id,name,start_date,end_date,description
      },{'Authorization': `Bearer ${this.token}`});
  }

  deleteResumeLine(id:number){
    return this.http.sendToServer('POST',API.EMPLOYEE.RESUMELINE.DELETE(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  deleteSkill(id){
      return this.http.sendToServer('POST',API.EMPLOYEE.EMPLOYEESKILL.DELETE(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  getEmployeeSkill(id){
    return this.http.sendToServer('GET',API.EMPLOYEE.EMPLOYEESKILL.VIEW(id),null,{'Authorization': `Bearer ${this.token}`});
}

}
