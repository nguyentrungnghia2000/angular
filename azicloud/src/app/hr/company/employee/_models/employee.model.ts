export class Employee{
  num: number;
  id: number;
  category_name: string;
  name: string;
  current_address: string;
  phone: string;
  filename: string;
  filepath: string;
}
