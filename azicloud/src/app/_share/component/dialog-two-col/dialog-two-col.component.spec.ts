import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogTwoColComponent } from './dialog-two-col.component';

describe('DialogTwoColComponent', () => {
  let component: DialogTwoColComponent;
  let fixture: ComponentFixture<DialogTwoColComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogTwoColComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogTwoColComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
