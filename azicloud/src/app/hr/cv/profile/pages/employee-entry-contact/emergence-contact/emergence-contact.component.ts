import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { Emergence } from '../../../_models/address';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';
import { ContactEntryService } from '../../../_services/contact-entry.service';
import { LoaderService } from 'src/app/_share/services/loader.service';

@Component({
  selector: 'app-emergence-contact',
  templateUrl: './emergence-contact.component.html',
  styleUrls: ['./emergence-contact.component.scss']
})
export class EmergenceContactComponent implements OnInit {
  emergences:Emergence;
  emergencesUpdate:Emergence;
  id_employee!: any;
  userId!: any;
  loadData = false;
  loading$ = this.loaderService.isLoading$;
  constructor(
    private contactEntryService: ContactEntryService,
    private primengConfig: PrimeNGConfig,
    public loaderService: LoaderService,
    private toastr: ToastrService,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private doc: Document) {
      this.emergences = new Emergence();
      this.userId = localStorage.getItem('user');
      this.id_employee = parseInt(this.userId);
    }

  ngOnInit(): void {
    this.getEmergency()
    this.primengConfig.ripple = true;
  }

  display: boolean = false;

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  showDialog(){
    this.emergencesUpdate = new Emergence();
    this.getEmergencyUpdate();
    this.updateBodyPadding();
    this.display = true;
  }

  closeDialog(){
    this.display = false;
    this.updateBodyPadding(true);
  }

  updateEmergency(){
    this.display = false;

    if(this.emergencesUpdate.emergency_address !== '' && this.emergencesUpdate.name !== '' && this.emergencesUpdate.emergency_relationship !== ''){
    this.contactEntryService.updateEmergency(this.id_employee, this.emergencesUpdate.emergency_address, this.emergencesUpdate.name,
                                            this.emergencesUpdate.emergency_relationship, this.emergencesUpdate.emergency_mobile, this.emergencesUpdate.emergency_phone).subscribe((res:any) => {
      if(res !== null){
        this.toastr.success('Cập nhật thành công', 'THÔNG BÁO');
        this.getEmergency()
        this.closeDialog();
      } else {
        this.toastr.error('Cập nhật thất bại', 'LỖI');
      }
    })} else {
      this.toastr.error('Vui lòng kiểm tra và nhập đầy đủ thông tin', 'LỖI');
    }
  }

  getEmergency() {
    this.loadData = true;
    this.contactEntryService.getEmergence(this.id_employee).subscribe((res:any) => {
      if(res !== null){
        this.emergences = res;
        this.loadData = false;
      }
    });
  }

  getEmergencyUpdate() {
    this.contactEntryService.getEmergence(this.id_employee).subscribe((res:any) => {
      if(res !== null){
        this.emergencesUpdate = res;
      }
    });
  }
}
