import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { User } from '../_models/user';
import { HttpService } from 'src/app/_share/services/http.service';
import { API } from 'src/app/_share/api/api.core.config';
import { of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  usercompany:any
  user!: User;
  token!: string;
  expire!: Date;
  isAuthenticated: boolean = false;

  constructor(
    private http: HttpService,
    private auth:AuthService,
    private toastr: ToastrService) {
    this.user = new User();
  }


  login(username: string, password: string, browser_uid: string) {

    return this.http.sendToServer('POST', API.USER.LOGIN, { username, password, browser_uid }).pipe(
      tap((res: any) => {
        if (res.status === 1) {
          this.user = res;
          localStorage.setItem('user', this.user.employee_id.toString());
          localStorage.setItem('user_id', this.user.id.toString());
          this.isLogin();
          this.setSession(res.access_token, res.refresh_token);
        }
      }
      )
    )
  }

  createBrowser(identifier: string, name: string, version: string, userAgent: string){

    return this.http.sendToServer('POST', API.USER.BROWSER, {
      identifier,
      name,
      version,
      userAgent
    }).pipe(tap((res: any) => {}))
  }

  private setSession(accessToken: string, refreshToken: string) {
    localStorage.setItem('access_token', accessToken);
    localStorage.setItem('refresh_token', refreshToken);
  }

  private isLogin() {
    this.isAuthenticated = true;
    localStorage.setItem('isAuthenticated', this.isAuthenticated.toString());
  }

  loginSocial(uid: string, username: string, menthod: string, browser_uid: string){
    return this.http.sendToServer('POST',API.USER.LOGIN_SOCIAL, {
      uid,
      username,
      menthod,
      browser_uid}).pipe(tap((res:any) => {
        if(res.status === 1){
          localStorage.setItem('user', JSON.stringify(this.user.employee_id));
          localStorage.setItem('user_id', JSON.stringify(this.user.id));
          this.isLogin();
          this.setSession(res.access_token, res.refresh_token);
        } else if(res.status === -2){
          this.toastr.error('Người dùng chưa đăng ký', 'LỖI');
        } else if(res.status === -3){
          this.toastr.error('Tài khoản đã liên kết và bị khóa', 'LỖI');
        } else if(res.status === -4){
          this.toastr.error('Tài khoản đã liên kết và người dùng đã bị khóa', 'LỖI');
        } else if(res.status === -5){
          this.toastr.error('Tài khoản đã liên kết nhưng chưa xác thực', 'LỖI');
        } else if(res.status === -6){
          this.toastr.error('Tài khoản chưa liên kết và người dùng đã xác thực', 'LỖI');
        } else if(res.status === -7){
          this.toastr.error('Tài khoản chưa liên kết và người dùng chưa xác thực', 'LỖI');
        } else if(res.status === -8){
          this.toastr.error('Tài khoản chưa liên kết và người dùng đã bị khóa', 'LỖI');
        }
      }))
  }

  renewOTP(username: string, type: string){
    return this.http.sendToServer('POST', API.USER.OTP_RENEW, {
      username,
      type
    });
  }

  availableOTP(username: string, token_otp: string){
    return this.http.sendToServer('POST', API.USER.OTP_AVAILABLE, {
      username,
      token_otp
    });
  }

  createCompany(user_id: number, name: string, address: string, url: string){
    return this.http.sendToServer('POST', API.USER.COMPANY_QUICK_CREATE, {
      user_id,
      name,
      address,
      url
    });
  }

  linkAccount(uid: string, username: string, menthod: string, user_id: string){
    return this.http.sendToServer('POST', API.USER.SOCIAL_CONFIRM, {
      uid,
      username,
      menthod,
      user_id
    });
  }

  linkOTP(user_id: string, username: string, uid: string, menthod: string, name: string, token_otp: string){
    return this.http.sendToServer('POST', API.USER.SOCIAL_CREATE, {
      user_id,
      username,
      uid,
      menthod,
      name,
      token_otp
    });
  }
}
