import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { User } from '../_models/user';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/_share/services/http.service';
import { API } from 'src/app/_share/api/api.core.config';

@Injectable({
  providedIn: 'root'
})
export class NewPasswordService {

  token_timeout: boolean = false;

  constructor(private http: HttpService,
              private router: Router) { }

  forgotPassword(username: string){
    return this.http.sendToServer('POST',API.USER.FORGOT_PASSWORD, {username});
  }

  tokenTimeout(username: string, token_timeout: string){

    return this.http.sendToServer('POST',API.USER.TOKEN_TIMEOUT, {
      username,
      token_timeout}).pipe(
      tap((res: any) => {
        if(res.result === 1){
          console.log('result = 1');
        }
      }))
  }

  changePassword(username: string, newpassword: string, token_otp: string){

    return this.http.sendToServer('POST',API.USER.CHANGE_PASSWORD ,{
      username,
      newpassword,
      token_otp});
  }
}
