import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class HttpService {
    get<T>(API: string, arg1: { headers: import("@angular/common/http").HttpHeaders; }): Observable<any> {
      throw new Error('Method not implemented.');
    }
    post(URL_LOGIN_FACEBOOK: any, arg1: { name: string; email: string; fb_uid: string; fb_token: string; browser_uid: string; }, httpOptions: { headers: any; }) {
      throw new Error('Method not implemented.');
    }

    token = localStorage.getItem('access_token');
    constructor(private http: HttpClient) { }

    public sendToServer(method: string, api: string, bd?: any, hd?: any, pr?: any) {
        let url = environment.endPoint + api;
        let ret: Observable<any>;
        let body = bd || {};
        let header = hd || { 'Content-Type': 'application/json', 'Authorization': `Bearer ${this.token}` };

        if (pr) Object.keys(pr).forEach(element => {
            if (pr[element] === '' || typeof (pr[element]) == 'undefined' || pr[element] == null) {
                delete pr[element];
            }
        });
        const params = new HttpParams({ fromObject: pr });
        console.log(url)
        switch (method) {
            case 'GET':
                ret = this.http.get(url, {
                    headers: header,
                    observe: 'body',
                    params: params
                });
                break;
            case 'POST':
                ret = this.http.post(url, body, {
                    headers: header,
                    observe: 'body',
                    params: params
                });
                break;
            case 'PATCH':
                ret = this.http.patch(url, body, {
                    headers: header,
                    observe: 'body',
                    params: params
                });
                break;
            case 'PUT':
                ret = this.http.put(url, body, {
                    headers: header,
                    observe: 'body',
                    params: params
                });
                break;
            case 'DELETE':
                ret = this.http.delete(url, {
                    headers: header,
                    observe: 'body',
                    params: params
                });
                break;
            default:
                break;
        }
        return new Observable(obs => {
            ret.subscribe(res => {
                obs.next(res);
                obs.complete();
            }, (err) => {
                // JSUtils.commonHandleError(err, this.msg);
                obs.error(err);
            });
        })
    }
}
