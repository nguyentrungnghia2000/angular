import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SocialLogin } from '../../_models/social-login';
import { LoginService } from '../../_services/login.service';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent implements OnInit {

  socialAccount: SocialLogin;
  method: string = '';
  user_id: string = '';
  name: string = '';

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private loginsService: LoginService
  ) {
    this.socialAccount = new SocialLogin();
    this.socialAccount = JSON.parse(localStorage.getItem('social_account'));
    this.user_id = localStorage.getItem('user_id');
    this.name = localStorage.getItem('name');
  }

  ngOnInit(): void {
  }

  acceptLink(){

    if(this.socialAccount.provider === 'GOOGLE'){
      this.method = 'google';
    } else if(this.socialAccount.provider === 'FACEBOOK'){
      this.method = 'facebook';
    }

    this.loginsService.linkAccount(this.socialAccount.id, this.socialAccount.email,
                                  this.method, this.user_id).subscribe((res: any)=>{
        if(res.result === 1){
          this.toastr.success('Liên kết thành công. Hãy xác nhận otp được gửi trong mail của bạn.', 'THÔNG BÁO');
          this.router.navigate(['/core/frontend/auth/link-otp']);
        } else if(res.result === -2){
          this.toastr.error('Tài khoản liên kết đã tồn tài', 'LỖI');
        } else if(res.result === -3){
          this.toastr.error('Tài khoản liên kết đã bị khóa', 'LỖI');
        } else if(res.result === -4){
          this.toastr.error('Tài khoản liên kết không tồn tài', 'LỖI');
        }
      })
  }

}
