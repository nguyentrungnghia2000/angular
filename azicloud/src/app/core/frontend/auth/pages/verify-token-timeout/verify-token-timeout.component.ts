import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NewPasswordService } from '../../_services/new-password.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../../_services/login.service';

@Component({
  selector: 'app-verify-token-timeout',
  templateUrl: './verify-token-timeout.component.html',
  styleUrls: ['./verify-token-timeout.component.scss']
})
export class VerifyTokenTimeoutComponent implements OnInit {

  code = new codeNumber();

  token_timeout: string = '';
  email: string = '';

  constructor(private newpassService: NewPasswordService,
              private router: Router,
              private route: ActivatedRoute,
              private toastr: ToastrService,
              private loginService: LoginService) {
                // this.route.queryParams.subscribe(params => {
                //   this.token_timeout = params['token_timeout'];
                // });
                this.code = new codeNumber();
              }

  ngOnInit(): void {
  }

  onClick(): void{

    this.email = localStorage.getItem('username');
    this.token_timeout = this.code.getCode();

    this.loginService.availableOTP(this.email, this.token_timeout).subscribe((res: any) => {
      if(res.result === 1){
        localStorage.setItem('token_timeout', this.token_timeout);
        this.toastr.success('Xác thực OTP thành công', 'THÀNH CÔNG');
        this.router.navigate(['/core/frontend/auth/change']);
      } else if(res.result === -1){
        this.toastr.error('Tài khoảng không tồn tại', 'LỖI');
      } else if(res.result === -2){
        this.toastr.warning('OTP không đúng. Xin kiểm tra lại', 'CẢNH BÁO');
      } else if(res.result === -3){
        this.toastr.error('OTP hết hạn', 'LỖI');
      } else if(res.result === -4){
        this.toastr.error('Tài khoản người dùng đã bị khóa', 'LỖI');
      }
    });
  }

  resendOTP(){
    let user_name = localStorage.getItem('username');
    this.loginService.renewOTP(user_name, 'string').subscribe((res: any) => {
      if(res.result === 1){
        this.toastr.success('Đã gửi lại mã OTP', 'THÔNG BÁO');
      }
    });
  }

  onKeyUp(event: any, pre: any, cur: any, next: any){
    let length = cur.value.length;
    let maxLength = cur.getAttribute('maxlength');
    if(length == maxLength){
      if(next != ""){
        next.focus();
      }
    }
    if(event.key === "Backspace"){
      if(pre != ""){
        pre.focus();
      }
    }
  }

}

export class codeNumber {
  first: string = '';
  second: string = '';
  third: string = '';
  fourth: string = '';
  fifth: string = '';
  sixth: string = '';

  public getCode() {
    const num = this.first + this.second + this.third + this.fourth + this.fifth + this.sixth;
    return num;
  }
}
