import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/_share/services/loader.service';

@Component({
  selector: 'app-employee-entry-info',
  templateUrl: './employee-entry-info.component.html',
  styleUrls: ['./employee-entry-info.component.scss']
})
export class EmployeeEntryInfoComponent implements OnInit {

  loading$ = this.loaderService.isLoading$;

  constructor(public loaderService: LoaderService,) { }

  ngOnInit(): void {
  }

}
