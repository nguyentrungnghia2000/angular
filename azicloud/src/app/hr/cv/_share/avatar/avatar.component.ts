import { HttpService } from './../../../../_share/services/http.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpEventType } from '@angular/common/http';
import { API } from 'src/app/_share/api/api.hr.config';
import { ReadVarExpr } from '@angular/compiler';
import { PrimeNGConfig } from 'primeng/api';
import { Avatar,Skill } from './_models/avatar';
import { InfoEntryService } from '../../profile/_services/info-entry.service';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { Company } from 'src/app/core/frontend/auth/_models/company.module';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {
  web_personal:Avatar;
  skill_list:Skill;
  fileupdate:File = null;
  id_employee!: any;
  userId!: any;
  user_ID: number;
  img:any;
  percentDone:number;
  token = '';

  userCompany: Company;

  loading$ = this.loaderService.isLoading$;

  constructor(private http:HttpService,
              private infoEntryService:InfoEntryService,
              private primengConfig: PrimeNGConfig,
              private toastr: ToastrService,
              public loaderService: LoaderService) {
      this.web_personal = new Avatar();
      this.skill_list = new Skill();
      let user_id = localStorage.getItem('user_id');
      this.user_ID = parseInt(user_id);
      this.userId = localStorage.getItem('user');
      this.id_employee = parseInt(this.userId);
      this.token = localStorage.getItem('access_token');
      this.userCompany = new Company();
    }

  ngOnInit(): void {
    this.getEmployeeInfo()
    this.primengConfig.ripple = true;
  }

  display: boolean = false
  showDialog(){
    this.display = true;
  }

  closeDialog(){
    this.display = false
  }

  onFileSelected(event:any){

    this.fileupdate = <File>event.target.files[0];
    let reader = new FileReader();
    reader.onload = (event: any) => {
      this.img = event.target.result;
    }
    reader.readAsDataURL(this.fileupdate);

    this.infoEntryService.uploadImage(this.user_ID, this.userCompany.url, this.fileupdate).subscribe((res: any) =>{
      if(res.result == 1){
        const image_id = res.id
        const image_type = "avatar"
        this.http.sendToServer('POST',API.EMPLOYEE.IMAGE_UPDATE(this.id_employee),{image_id,image_type},{'Authorization':`Bearer ${this.token}`})
        .subscribe((result: any) =>{
          if(result.result === 2){
            this.getEmployeeInfo();
            this.toastr.success('Cập nhật ảnh đại diện thành công', 'THÔNG BÁO');
          } else {
            this.toastr.error('Cập nhật ảnh đại diện thất bại', 'LỖI');
          }
        })
      }
    });
  }

  getEmployeeInfo() {

     this.http.sendToServer('GET',API.EMPLOYEE.WEB_PERSONAL_VIEW(this.id_employee),null,{'Authorization':`Bearer ${this.token}`})
    .subscribe((res:any) => {
      this.web_personal = res
      this.img = `http://api.azicloud.vn/${res.image_filepath}${res.image_filename}`

    })
    this.http.sendToServer('GET',API.EMPLOYEE.SKILL_LIST(this.id_employee),null,{'Authorization':`Bearer ${this.token}`})
      .subscribe((res:any) => {
        this.skill_list = res;
    });

  }
}
