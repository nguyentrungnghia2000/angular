export class EmployeeHealth{
  id!: number;
  height!: number;
  weight!: number;
  blood_group!: string;
  blood_key!: string;
  congenital_diseases!: string;
  status!: string;
  check_date!: string;
  note!: string;
}
