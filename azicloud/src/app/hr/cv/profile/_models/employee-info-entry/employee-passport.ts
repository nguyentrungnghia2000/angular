export class Passport{
  id!: number;
  passport!: string;
  passport_start_date!: string;
  passport_end_date!: string;
  passport_issued!: string;
  passport_front_image_id!: number;
  passport_front_image_filepath!: string;
  passport_front_image_filename!: string;
  passport_backside_image_id!: number;
  passport_backside_image_filepath!: string;
  passport_backside_image_filename!: string;
}
