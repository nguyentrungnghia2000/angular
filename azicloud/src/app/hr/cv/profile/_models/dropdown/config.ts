export class Configs{
  value!: string;
  key!: string;

  constructor(value: string, key: string){
    this.value = value;
    this.key = key;
  }
}
