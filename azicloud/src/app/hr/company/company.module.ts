import { NgModule } from "@angular/core";
import { CompanyRoutingModule } from "./company-routing.module";
import { ShareModule } from "./_share/share.module";
import { CompanyComponent } from "./company.component";
import { EmployeeComponent } from './employee/employee.component';
@NgModule({
    declarations:[
      CompanyComponent,
    ],
    imports:[
        ShareModule,
        CompanyRoutingModule
    ],
    exports:[]
})

export class CompanyModule {}
