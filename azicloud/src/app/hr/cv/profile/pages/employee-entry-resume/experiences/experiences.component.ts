import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { debounceTime, map } from 'rxjs/operators';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { Country } from '../../../_models/dropdown/country';
import { Experiences } from '../../../_models/employee-resume-entry/experiences.model';
import { ResumeService } from '../_services/resume.service';

@Component({
  selector: 'app-experiences',
  templateUrl: './experiences.component.html',
  styleUrls: ['./experiences.component.scss']
})
export class ExperiencesComponent implements OnInit {
  displayBasic: boolean;
  displayBasicupdate: boolean;
  deleteDisplay = false;
  experience_id = 0;
  id_employee!: any;
  userId!: any;
  startDate: any;
  endDate: any;
  startDateChecked = false;
  endDateChecked = false;

  experienceType: any[] = [];
  experience: any[] = [];
  ListRessumeLineType: Country[] =[];
  ResumeLineType: Country;

  experienceCreate: Experiences;
  experienceUpdate: Experiences;
  loadData = false;

  loading$ = this.loaderService.isLoading$;

  constructor(private resumeService: ResumeService,
              private toastr: ToastrService,
              public loaderService: LoaderService,
              private renderer: Renderer2,
              @Inject(DOCUMENT) private doc: Document) {
      this.userId = localStorage.getItem('user');
      this.id_employee = parseInt(this.userId);
      this.experienceCreate = new Experiences();
    }

  ngOnInit(): void {
    this.getdataExperience();
    this.loadListResumeLineType();
  }

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  loadListResumeLineType(){
    this.resumeService.getResumeLineType(1).pipe(map((result: any)=>
    result.map((data: any) =>{
      return this.ListRessumeLineType.push(this.ResumeLineType = new Country(data.name, data.id));

    }))).subscribe((res: any) =>{
      if(res !== null){
      }
      else{
        this.toastr.error('Lỗi', 'Tải danh sách thất bại');
      }
    })
  }

  getdataExperience(){
  this.loadData = true
  this.resumeService.getExperience(this.id_employee).subscribe((data: any) => {
      if(data !== null){
        this.experienceType = data.types;
        for(let i = 0; i < this.experienceType.length; i++){
          for(let j = 0; j < this.experienceType[i].resume_lines.length; j++){
            this.experienceType[i].resume_lines[j].start_date = this.getDateOnly(this.experienceType[i].resume_lines[j].start_date);
            this.experienceType[i].resume_lines[j].end_date = this.getDateOnly(this.experienceType[i].resume_lines[j].end_date);
          }
        }
        // this.experience = data.types.resume_lines;
        // console.log(this.experience);
        // for(let i = 0; i < this.experience.length; i++){
        //   this.experience[i].start_date = this.getDateOnly(this.experience[i].start_date);
        //   this.experience[i].end_date = this.getDateOnly(this.experience[i].end_date);
        // }
        this.loadData = false
      }
    })
  }

  showModalDialog() {
    this.experienceCreate = new Experiences();
    this.displayBasic = true;
    this.updateBodyPadding();
  }

  showdialogupdate(id) {

    this.experienceUpdate = new Experiences();
    this.displayBasicupdate = true;
    this.updateBodyPadding();
    this.resumeService.getResumeline(id).subscribe((x:any) =>{
      if(x !== null){
        this.experienceUpdate = x;
        this.startDate = new Date(this.experienceUpdate.start_date);
        this.endDate = new Date(this.experienceUpdate.end_date);
      }else{
        this.toastr.error('Không thể hiển thị thông tin kinh nghiệm', 'LỖI');
      }
    })
  }

  displayDialogDelete(experience_id:number){
    this.deleteDisplay = true;
    this.experience_id = experience_id;
    this.updateBodyPadding();
  }

  closeDialog(){
    this.displayBasic = false;
    this.displayBasicupdate = false;
    this.deleteDisplay = false;
    this.updateBodyPadding(true);
  }

  deleteResume(){
    if(this.experience_id !== 0){
      this.resumeService.deleteResumeLine(this.experience_id).subscribe((res:any) => {
        if(res.result === 1){
          this.toastr.success('Xóa thành công', 'THÔNG BÁO');
          this.getdataExperience();
          this.experience_id = 0;
          this.closeDialog();
        } else {
          this.toastr.error('Xóa thất bại', 'LỖI');
        }
      });
    }

  }

  createResumeLine() {
    this.displayBasic = false;
    if(this.experienceCreate.name !== undefined &&
      this.experienceCreate.start_date !== undefined &&
      this.experienceCreate.end_date !== undefined &&
      this.experienceCreate.description !== undefined){

      this.resumeService.createResumeLine(this.id_employee, -1, this.experienceCreate.type_id,
                                          this.experienceCreate.name, this.experienceCreate.start_date,
                                          this.experienceCreate.end_date, this.experienceCreate.description).subscribe(x => {
        if(x !== null){
          this.toastr.success('Thêm mới kinh nghiệm thành công', 'THÔNG BÁO');
          this.getdataExperience();
          this.closeDialog();
        } else {
          this.toastr.error('Thêm mới kinh nghiệm thất bại', 'LỖI');
        }
      })
    } else {
      this.toastr.warning('Chưa nhập đầy đủ thông tin. Vui lòng kiểm tra lại', 'CẢNH BÁO');
    }

  }

  updateResumeLine() {
    this.displayBasicupdate = false;

    if(this.experienceUpdate.end_date !== undefined){
    this.resumeService.createResumeLine(this.id_employee, this.experienceUpdate.id, this.experienceUpdate.type_id,
                                        this.experienceUpdate.name, this.experienceUpdate.start_date,
                                        this.experienceUpdate.end_date, this.experienceUpdate.description).subscribe(x => {
        if(x !== null){
          this.toastr.success('Cập nhật kinh nghiệm thành công', 'THÔNG BÁO');
          this.getdataExperience();
          this.closeDialog();
        } else {
          this.toastr.error('Cập nhật kinh nghiệm thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Xin hãy kiểm tra lại thông tin cập nhật', 'LỖI');
    }
  }

  //split date to show on label
  getDateOnly(str: string){
    let arrTemp: string[] = str.split('T');
    return arrTemp[0];
  }

  //get experienceUpdate.type_id from experienceUpdate.type_name
  onSelectedUpdate(e) {
    let changedValue = e;
    for(let i = 0; i < this.ListRessumeLineType.length; i++){
      if(this.ListRessumeLineType[i].name === changedValue){
        this.experienceUpdate.type_id = this.ListRessumeLineType[i].id;
      }
    }
  }

  //get experienceCreate.type_id from experienceCreate.type_name
  onSelectedCreate(e) {
    let changedValue = e;
    for(let i = 0; i < this.ListRessumeLineType.length; i++){
      if(this.ListRessumeLineType[i].name === changedValue){
        this.experienceCreate.type_id = this.ListRessumeLineType[i].id;
      }
    }
  }

  // convert start date create from datepicker to tring
  onSelectedStartdate(e) {
    this.experienceCreate.start_date = new Date(Date.parse(e)).toISOString();
  }

  // convert end date create from datepicker to tring
  onSelectedEnddate(e) {
    let temp = new Date(Date.parse(e)).toISOString();
    if(temp >= this.experienceCreate.start_date){
      this.experienceCreate.end_date = new Date(Date.parse(e)).toISOString();
    } else {
      this.experienceUpdate.end_date = undefined;
      this.toastr.error('Ngày kết thúc phải lớn hơn ngày bắt đầu. Xin hãy chọn lại', 'LỖI');
    }
  }

  // convert start date update from datepicker to tring
  onSelectedStartdateUpdate(e) {
    this.experienceUpdate.start_date = new Date(Date.parse(e)).toISOString();
  }

  // convert end date update from datepicker to tring
  onSelectedEnddateUpdate(e) {
    let temp = new Date(Date.parse(e)).toISOString();
    if(temp >= this.experienceUpdate.start_date){
      this.experienceUpdate.end_date = new Date(Date.parse(e)).toISOString();
    } else {
      this.experienceUpdate.end_date = undefined;
      this.toastr.error('Ngày kết thúc phải lớn hơn ngày bắt đầu. Xin hãy chọn lại', 'LỖI');
    }
  }

}
