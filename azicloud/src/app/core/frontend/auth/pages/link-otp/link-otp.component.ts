import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SocialLogin } from '../../_models/social-login';
import { AuthService } from '../../_services/auth.service';
import { LoginService } from '../../_services/login.service';

@Component({
  selector: 'app-link-otp',
  templateUrl: './link-otp.component.html',
  styleUrls: ['./link-otp.component.scss']
})
export class LinkOtpComponent implements OnInit {

  code = new codeNumber();

  socialAccount: SocialLogin;
  method: string = '';
  user_id: string = '';
  name: string = '';
  user_name = '';
  token_otp = '';

  constructor(private router: Router,
    private auth: AuthService,
    private loginService: LoginService,
    private toastr: ToastrService) {
      this.code = new codeNumber();
      this.socialAccount = JSON.parse(localStorage.getItem('social_account'));
      this.user_id = localStorage.getItem('user_id');
      this.name = localStorage.getItem('name');
    }

  ngOnInit(): void {
  }

  verifyOTP() {
    this.token_otp = this.code.getCode();
    if(this.socialAccount.provider === 'GOOGLE'){
      this.method = 'google';
    } else if(this.socialAccount.provider === 'FACEBOOK'){
      this.method = 'facebook';
    }
    this.loginService.linkOTP(this.user_id, this.socialAccount.email,
                              this.socialAccount.id, this.method,
                              this.name, this.token_otp).subscribe((res: any) => {
      if(res.result === 1){
        this.toastr.success('Xác thực OTP thành công', 'THÔNG BÁO');
        localStorage.removeItem('user_name');
        localStorage.removeItem('name');
        localStorage.removeItem('social_account');
        localStorage.removeItem('user_id');
        this.router.navigate(['/core/frontend/auth/login']);
      } else if(res.result === -1){
        this.toastr.error('Tài khoảng không tồn tại', 'LỖI');
      } else if(res.result === -2){
        this.toastr.warning('OTP không đúng. Xin kiểm tra lại', 'CẢNH BÁO');
      } else if(res.result === -3){
        this.toastr.error('OTP hết hạn', 'LỖI');
      } else if(res.result === -4){
        this.toastr.error('Tài khoản người dùng đã bị khóa', 'LỖI');
      }
    });

  }

  onKeyUp(event: any, pre: any, cur: any, next: any){
    let length = cur.value.length;
    let maxLength = cur.getAttribute('maxlength');
    if(length == maxLength){
      if(next != ""){
        next.focus();
      }
    }
    if(event.key === "Backspace"){
      if(pre != ""){
        pre.focus();
      }
    }
  }

  resendOTP(){
    let user_name = localStorage.getItem('user_name');
    this.loginService.renewOTP(user_name, 'string').subscribe((res: any) => {
      if(res.result === 1){
        this.toastr.success('Đã gửi lại mã OTP', 'THÔNG BÁO');
      }
    });
  }
}

export class codeNumber {
  first: string = '';
  second: string = '';
  third: string = '';
  fourth: string = '';
  fifth: string = '';
  sixth: string = '';

  public getCode() {
    const num = this.first + this.second + this.third + this.fourth + this.fifth + this.sixth;
    return num;
  }
}

