import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { PrimeNGConfig } from 'primeng/api';
import { Current } from '../../../_models/address';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';
import { ContactEntryService } from '../../../_services/contact-entry.service';
import { Country } from '../../../_models/dropdown/country';
import { concatMap } from 'rxjs/operators';
import { LoaderService } from 'src/app/_share/services/loader.service';

@Component({
  selector: 'app-current-place',
  templateUrl: './current-place.component.html',
  styleUrls: ['./current-place.component.scss']
})

export class CurrentPlaceComponent implements OnInit {
  id_employee!: any;
  userId!: any;
  currunts:Current;
  curruntsUpdate:Current;
  country_id:number;
  area_id:number;
  territory_id:number;
  subterritory_id:number;
  loadData = false;

  country_list: Country[] =[];
  contry: Country;
  area_list: Country[] =[];
  area: Country;
  territory_list: Country[] =[];
  territory: Country;
  subterritory_list: Country[] =[];
  subterritory: Country;

  countryChange = false;
  areaChange = false;
  territoryChange = false;
  subterritoryChange = false;
  loading$ = this.loaderService.isLoading$;

   constructor(
            private primengConfig: PrimeNGConfig,
            private contactEntryService: ContactEntryService,
            private toastr: ToastrService,
            public loaderService: LoaderService,
            private renderer: Renderer2,
            @Inject(DOCUMENT) private doc: Document) {
            this.currunts = new Current();
            this.userId = localStorage.getItem('user');
            this.id_employee = parseInt(this.userId);
            }

  ngOnInit(): void {
    this.getCurrent();
    this.getCountryList();
    this.primengConfig.ripple = true;
  }

  display: boolean = false;

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  showDialog(){
    this.curruntsUpdate = new Current();
    this.getCurrentUpdate();
    this.updateBodyPadding();
    this.display = true;
  }

  closeDialog(){
    this.display = false;
    this.updateBodyPadding(true);
  }

  updateCurrent(){

    if(this.countryChange === false) { this.country_id = this.curruntsUpdate.current_country_id; }
    if(this.areaChange === false) { this.area_id = this.curruntsUpdate.current_area_id; }
    if(this.territoryChange === false) { this.territory_id = this.curruntsUpdate.current_territory_id; }
    if(this.subterritoryChange === false) { this.subterritory_id = this.curruntsUpdate.current_subterritory_id; }

    if(this.country_id !== undefined && this.area_id !== undefined && this.territory_id !== undefined &&
      this.subterritory_id !== undefined && this.curruntsUpdate.current_address !== ''){

      this.contactEntryService.updateCurrenPlace(this.id_employee, this.curruntsUpdate.current_address, this.curruntsUpdate.phone,
                                                  this.curruntsUpdate.mobile, this.country_id, this.area_id,
                                                  this.territory_id, this.subterritory_id).subscribe((res:any) => {
        if(res.result === 1){
          this.toastr.success('Cập nhật thành công', 'THÔNG BÁO');
          this.display = false;
          this.countryChange = false;
          this.areaChange = false;
          this.territoryChange = false;
          this.subterritoryChange = false;
          this.getCurrent();
          this.closeDialog();
        } else{
          this.toastr.error('Cập nhật thất bại', 'LỖI');
        }
      });} else {
        this.toastr.error('Vui lòng kiểm tra và nhập đầy đủ thông tin', 'LỖI');
      }
  }


  getCurrent() {
    this.loadData = true;
    this.contactEntryService.getCurrentPlace(this.id_employee).subscribe((res:any) => {
      if(res !== null){
        this.currunts = res;
        this.loadData = false;
      }
    });
  }

  getCurrentUpdate() {
    this.contactEntryService.getCurrentPlace(this.id_employee).subscribe((res:any) => {
      if(res !== null){
        this.curruntsUpdate = res;
        this.loadAllListPlace(this.currunts.current_country_id, this.currunts.current_area_id, this.currunts.current_territory_id);
      }
    });
  }

  getCountryList(){

    this.country_list = [];
    this.contactEntryService.getCountry().pipe(map((result: any)=>
    result.map((data: any) =>{
      return this.country_list.push(this.contry = new Country(data.name, data.id));
    }))).subscribe((res: any) =>{
      if(res !== null){
      }
      else{
        this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
      }
    })

  }

  //get list area from country_id
  getAreaList(event:any):void{
    if(event !== undefined){
      this.countryChange = true;
      //reset area_list to load new list
      this.area_list = [];
      //check if new change country id then remove data behind
      this.country_id = this.country_list.find(data => data.name === event)?.id;
      if(this.country_id !== this.curruntsUpdate.current_country_id){
        this.curruntsUpdate.current_area_name = '';
        this.curruntsUpdate.current_area_id = 0;
        this.curruntsUpdate.current_territory_name = '';
        this.curruntsUpdate.current_territory_id = 0;
        this.curruntsUpdate.current_subterritory_name = '';
        this.curruntsUpdate.current_subterritory_id = 0;
      }
      //call api and map data
      this.contactEntryService.getArea(this.country_id).pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.area_list.push(this.area = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res === null){
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy country_id', 'LỖI');
    }
  }

  // get list territory from area_id
  getTerritoryList(event:any):void{
    if(event !== undefined){
      this.areaChange = true;
      //reset area_list to load new list
      this.territory_list = [];
      //check if new change country id then remove data behind
      this.area_id = this.area_list.find(data => data.name === event)?.id;
      if(this.area_id !== this.curruntsUpdate.current_area_id){
        this.curruntsUpdate.current_territory_name = '';
        this.curruntsUpdate.current_territory_id = 0;
        this.curruntsUpdate.current_subterritory_name = '';
        this.curruntsUpdate.current_subterritory_id = 0;
      }
      //call api and map data
      this.contactEntryService.getTerritory(this.area_id).pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.territory_list.push(this.territory = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res === null){
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy area_id', 'LỖI');
    }
  }

  // get list subterritory from territory_id
  getSubterritoryList(event:any):void{
    if(event !== undefined){
      this.territoryChange = true;
      //reset area_list to load new list
      this.subterritory_list = [];
      //check if new change country id then remove data behind
      this.territory_id = this.territory_list.find(data => data.name === event)?.id;
      if(this.territory_id !== this.curruntsUpdate.current_territory_id){
        this.curruntsUpdate.current_subterritory_name = '';
        this.curruntsUpdate.current_subterritory_id = 0;
      }
      //call api and map data
      this.contactEntryService.getSubterritory(this.territory_id).pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.subterritory_list.push(this.subterritory = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res === null){
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy territory_id', 'LỖI');
    }
  }

  //get subterritory_id
  getIdSubterritory(event:any):void{
    if(event !== undefined){
      this.subterritoryChange = true;
      this.subterritory_id = this.subterritory_list.find(data => data.name === event)?.id;
    } else {
      this.toastr.error('Không tìm thấy subterritory_id', 'LỖI');
    }
  }

  loadAllListPlace(country_id: number, area_id: number, territory_id: number){
    this.contactEntryService.getArea(country_id).pipe(
      tap((res: any) => res.map((data: any) => {
        return this.area_list.push(this.area = new Country(data.name, data.id));
      })),
      concatMap((res: any) => this.contactEntryService.getTerritory(area_id)),
      tap((res: any) => res.map((data: any) => {
        return this.territory_list.push(this.territory = new Country(data.name, data.id));
      })),
      concatMap((res: any) => this.contactEntryService.getSubterritory(territory_id)),
      tap((res: any) => res.map((data: any) => {
        return this.subterritory_list.push(this.subterritory = new Country(data.name, data.id));
      })),
    ).subscribe((data: any) => {
      if(data !== null){
        console.log('Load thông tin thành công');
      }
    })
  }


}
