import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {

  loading$ = this.loaderService.isLoading$;
  constructor(public loaderService: LoaderService) { }

  ngOnInit(): void {
  }

}
