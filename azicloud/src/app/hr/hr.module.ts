
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HrRoutingModule } from "./hr-routing.module";
import { HrComponent } from "./hr.component";
import { ShareModule } from "./_share/share.module";
@NgModule({
    declarations:[
      HrComponent
    ],
    imports:[
        CommonModule,
        HrRoutingModule,
        ShareModule

    ],
    exports:[]

})

export class HrModule {}
