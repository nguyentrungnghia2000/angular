export class Country{
  name!: string;
  id!: number;

  constructor(name: string, id: number){
    this.name = name;
    this.id = id;
  }
}
export class CountryProcess{
  name!: string;
  id!: number;
  level_process!:number;
  constructor(name: string, id: number,level_process:number){
    this.name = name;
    this.id = id;
    this.level_process = level_process
  }
}
