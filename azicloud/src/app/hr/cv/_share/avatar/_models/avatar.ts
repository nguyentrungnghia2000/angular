export class Avatar{
category_name: string;
current_address: string;
description_infor: string;
email: string;
id: number;
image_filename: string;
image_filepath: string;
image_id: number;
name: string;
phone: number;
}
export class Skill{
  skill_list: string;
  skill_types: string;
  }
