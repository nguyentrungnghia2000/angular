import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService, PrimeNGConfig } from 'primeng/api';
import { map, switchMap, tap } from 'rxjs/operators';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { Country,CountryProcess } from '../../../_models/dropdown/country';
import { Skills } from '../../../_models/employee-resume-entry/skills.model';
import { EmployeeSkill } from '../_model/employeeSkill';
import { Skill } from '../_model/skill';
import { ResumeService } from '../_services/resume.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: SkillsComponent,
    multi: true
  }]
})
export class SkillsComponent implements OnInit {

  displayBasic: boolean;
  visible: boolean;
  deleteDisplay = false;
  skill_id = 0;
  id_employee!: any;
  userId!: any;

  // % of process bar
  process: number;
  employeeSkills: EmployeeSkill[] = [];
  employeeSkillTypes: EmployeeSkill[] = [];
  employeeSkillList:any;
  addForm: FormGroup;

  EmSkillTypes: Country[] = [];
  EmSkillType: Country;
  skillLists: Country[] = [];
  skillList: Country;
  skillLevels: CountryProcess[]= [];
  skillLevel: CountryProcess;
  skillCreate: Skills;
  skillUpdate: Skills;

  loadData = false;
  idupdate: number = 0;
  skillLevelIdUpdate: number;
  skillTypeIdUpdate: number;

  loading$ = this.loaderService.isLoading$;

  constructor(private resumeService: ResumeService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public loaderService: LoaderService,
    private confirmationService: ConfirmationService,
    private primengConfig: PrimeNGConfig,
    private toastr: ToastrService,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private doc: Document) {
      this.userId = localStorage.getItem('user');
      this.id_employee = parseInt(this.userId);
      this.skillCreate = new Skills();
     }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.loadEmployeeSkillList();
    this.Getskill();
  }

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  // get list skill_type, skill, skill_level
  Getskill(){
    this.resumeService.getSkillType(1).pipe(map((result: any) =>
    result.map((data: any) => {
      return this.EmSkillTypes.push(this.EmSkillType = new Country(data.name, data.id));
    }))).subscribe((res: any) =>{
      if(res === null){
        this.toastr.error('Lỗi tải danh sách loại kỹ năng', 'LỖI');
      }
    })

    this.resumeService.getSkillList(1).pipe(map((result: any) =>
    result.map((data: any) => {
      return this.skillLists.push(this.skillList = new Country(data.name, data.id));
    }))).subscribe((res: any) =>{
      if(res === null){
        this.toastr.error('Lỗi tải danh sách kỹ năng', 'LỖI');
      }
    })

    this.resumeService.getSkillLevel(1).pipe(map((result: any) =>
    result.map((data: any) => {
      return this.skillLevels.push(this.skillLevel = new CountryProcess(data.name, data.id,data.level_process));
    }))).subscribe((res: any) =>{
      if(res === null){
        this.toastr.error('Lỗi tải danh sách level', 'LỖI');
      }
    })
  }

  loadEmployeeSkillList(){
    this.loadData = true
    this.resumeService.getEmployeeSkillList(this.id_employee).subscribe((data: any) => {
      if(data !== null){
        this.employeeSkills = data.skill_types;
        console.log(this.employeeSkills);
        for(let i = 0; i < this.employeeSkills.length; i++){
          console.log(this.employeeSkills[i].skill_list[0]);
        }
        this.employeeSkillList = data.skill_list
        this.loadData = false;
      }
    })
  }

  //create new skill
  add() {
    this.displayBasic = false;
    if(this.skillCreate.skill_id !== undefined && this.skillCreate.skill_type_id !== undefined && this.skillCreate.skill_level_id !== undefined){
      this.resumeService.createSkill(this.id_employee, -1,
                                    this.skillCreate.skill_id, this.skillCreate.skill_type_id,
                                    this.skillCreate.skill_level_id).subscribe(x => {
        if(x !== null){
          this.toastr.success('Thêm thành công', 'THÔNG BÁO');
          this.loadEmployeeSkillList();
          this.closeDialog();
        } else{
          this.toastr.error('Thêm mới kỹ năng thất bại', 'LỖI');
        }
      })
    } else {
      this.toastr.warning('Chưa nhập đầy đủ thông tin. Vui lòng kiểm tra lại', 'CẢNH BÁO');
    }
  }

  updateSkill() {
     this.resumeService.createSkill(this.id_employee, this.skillUpdate.id,
                                    this.skillUpdate.skill_id, this.skillUpdate.skill_type_id,
                                    this.skillUpdate.skill_level_id).subscribe( x => {
      if(x !== null){
        this.toastr.success('Cập nhật thành công', 'THÔNG BÁO');
        this.loadEmployeeSkillList();
        this.closeDialog();
      } else{
        this.toastr.error('Cập nhật kỹ năng thất bại', 'LỖI');
      }
     })
  }

  deleteSkill(){

    if(this.skill_id !== 0){
      this.resumeService.deleteSkill(this.skill_id).subscribe((res: any) => {
        if(res.result === 1){
          this.skill_id = 0;
          this.toastr.success('Xóa thành công', 'THÔNG BÁO');
          this.loadEmployeeSkillList();
          this.closeDialog();
        } else {
          this.toastr.error('Xóa thất bại', 'LỖI');
        }
      });
    }

  }

  displayDialog(id: any) {
    this.skillUpdate = new Skills();
    this.idupdate = id;
    this.resumeService.getEmployeeSkill(id).subscribe((res: any) => {
      if(res !== null){
        this.skillUpdate = res;
      }
    })
    this.visible = true;
    this.updateBodyPadding();
  }

  showModalDialog() {
    this.skillCreate = new Skills();
    this.displayBasic = true;
    this.updateBodyPadding();
  }

  displayDialogDelete(skill_id:number){
    this.deleteDisplay = true;
    this.skill_id = skill_id;
  }

  closeDialog(){

    this.updateBodyPadding(true);
    this.displayBasic = false;
    this.visible=false;
    this.deleteDisplay = false;
  }

  // get skill type id from name (create)
  changeidtypeCreate(event){
    for(let i = 0; i < this.EmSkillTypes.length; i++){
      if(this.EmSkillTypes[i].name === event){
        this.skillCreate.skill_type_id = this.EmSkillTypes[i].id;
      }
    }
  }

  // get skill id from name (create)
  changeid(event){
    for(let i = 0; i < this.skillLists.length; i++){
      if(this.skillLists[i].name === event){
        this.skillCreate.skill_id = this.skillLists[i].id;
      }
    }
  }

  // get skill level id from name (create)
  changeidlevel(event){
    this.skillCreate.level_process = this.skillLevels.find(data => data.name === event)?.level_process;
    for(let i = 0; i < this.skillLevels.length; i++){
      if(this.skillLevels[i].name === event){
        this.skillCreate.skill_level_id = this.skillLevels[i].id;
      }
    }
  }

  // get skill type id from name (update)
  changeidtypeUpdate(event){
    for(let i = 0; i < this.EmSkillTypes.length; i++){
      if(this.EmSkillTypes[i].name === event){
        this.skillUpdate.skill_type_id = this.EmSkillTypes[i].id;
      }
    }
  }

  // get skill id from name (update)
  changeidUpdate(event){
    for(let i = 0; i < this.skillLists.length; i++){
      if(this.skillLists[i].name === event){
        this.skillUpdate.skill_id = this.skillLists[i].id;
      }
    }
  }

  // get skill level id from name (update)
  changeidlevelUpdate(event){
    this.skillUpdate.level_process = this.skillLevels.find(data => data.name === event)?.level_process;
    for(let i = 0; i < this.skillLevels.length; i++){
      if(this.skillLevels[i].name === event){
        this.skillUpdate.skill_level_id = this.skillLevels[i].id;
      }
    }
  }

}
