export class Employee{
  id!: string;
  nationality_name!: string;
  gender_name!: string;
  marital_status_name!: string;
  nation_name!: string;
  name!: string;
  phone!: string;
  email!: string;
  tax_code!: string;
  birth_day!: number;
  birth_month!: number;
  birth_year!: number;
  nationality_id!: number;
  religion_name!: string;
  nation_key!: string;
  gender_key!: string;
  marital_status_key!: string;
  active!: boolean;
}
