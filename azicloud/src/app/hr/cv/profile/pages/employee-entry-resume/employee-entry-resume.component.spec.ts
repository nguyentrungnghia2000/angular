import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeEntryResumeComponent } from './employee-entry-resume.component';

describe('EmployeeEntryResumeComponent', () => {
  let component: EmployeeEntryResumeComponent;
  let fixture: ComponentFixture<EmployeeEntryResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeEntryResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeEntryResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
