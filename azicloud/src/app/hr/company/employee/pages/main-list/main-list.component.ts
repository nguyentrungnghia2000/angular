import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { Employee } from '../../_models/employee.model';
import { MainService } from '../../_services/main.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-list',
  templateUrl: './main-list.component.html',
  styleUrls: ['./main-list.component.scss']
})
export class MainListComponent implements OnInit {
  total_count: number = 0;
  list_item: Employee[] = [];
  currentList: Employee[] = [];
  item: Employee;
  startLoad: number = 0;
  endLoad: number = 0;
  currentPageNum: number = 0;
  search_text: string = '';

  constructor(private mainService: MainService,
              @Inject(DOCUMENT) private doc: Document,
              private renderer: Renderer2,
              private activatedRoute: ActivatedRoute,
              private router: Router) {

                this.splitUrl(this.router.url);
              }

  ngOnInit(): void {
    this.LoadListEmployee();

  }

  LoadListEmployee(){
    this.activatedRoute.queryParams
    .subscribe(params => {
        console.log(params.text);
        this.search_text = params.text;
    });
    if(this.search_text !== ''){
      this.mainService.getListEmployee(this.search_text).subscribe((res: any) => {
        if(res !== null){
          this.list_item = res.list_item;
          this.checkEndLoad(this.list_item.length, this.endLoad);
          this.currentPageListEmployee(this.list_item);
        }
      })
    } else {
      this.mainService.getListEmployeeWithoutText().subscribe((res: any) => {
        if(res !== null){
          this.list_item = res.list_item;
          this.checkEndLoad(this.list_item.length, this.endLoad);
          this.currentPageListEmployee(this.list_item);
        }
      })
    }
  }

  splitUrl(url){
    let arr = [];
    arr = url.split("=");
    if(arr.length > 1){
      this.currentPageNum = parseInt(arr[1]);
      this.endLoad = this.currentPageNum * 20;
      this.startLoad = this.endLoad - 20;
    } else{
      this.currentPageNum = 1;
      this.endLoad = 20;
      this.startLoad = 0;
    }
  }

  currentPageListEmployee(listE: Employee[]){
    for(let i = this.startLoad; i < this.endLoad; i++){
      this.currentList.push(listE[i]);
    }
  }

  checkEndLoad(list_length, endload){
    if(list_length < endload - 1){
      this.endLoad = list_length;
      this.startLoad = 20 * (this.currentPageNum - 1);
    }
  }

}
