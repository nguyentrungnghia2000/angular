
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CoreComponent } from "./core.component";


const routes: Routes = [
    { path:'', component: CoreComponent, children: [
            { path:'frontend', loadChildren: () => import('./frontend/frontend.module').then( m => m.FrontendModule) },
            { path:'', pathMatch:'full', redirectTo:'frontend' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class CoreRoutingModule {

}
