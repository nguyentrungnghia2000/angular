import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HrComponent } from "./hr.component";


const routes: Routes = [
    { path:'', component:HrComponent,
      children: [
            { path:'cv', loadChildren: () => import('./cv/cv.module').then( m => m.CvModule) },
            { path:'company', loadChildren: () => import('./company/company.module').then( m => m.CompanyModule) },
            { path:'', pathMatch:'full', redirectTo:'company' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class HrRoutingModule {

}
