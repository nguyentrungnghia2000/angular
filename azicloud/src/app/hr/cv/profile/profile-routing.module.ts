import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { EmployeeEntryContactComponent } from "./pages/employee-entry-contact/employee-entry-contact.component";
import { EmployeeEntryInfoComponent } from "./pages/employee-entry-info/employee-entry-info.component";
import { EmployeeEntryResumeComponent } from "./pages/employee-entry-resume/employee-entry-resume.component";
import { ProfileComponent } from "./profile.component";

const routes: Routes =[
    { path:'', component:ProfileComponent,
      children:[
        { path: 'contact', component: EmployeeEntryContactComponent},
        { path: 'info', component: EmployeeEntryInfoComponent},
        { path: 'resume', component: EmployeeEntryResumeComponent},
        { path:'', pathMatch:'full', redirectTo:'info' }
      ]
    }
]
@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class ProfileRoutingModule {}
