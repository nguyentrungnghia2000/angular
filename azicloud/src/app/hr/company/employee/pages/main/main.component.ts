import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../../_models/employee.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  @Input() employee_unit!: Employee;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  moveToInfo(){
    console.log('moved');
    this.router.navigate(['/hr/cv/profile/info']);
  }

}
