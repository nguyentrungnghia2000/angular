export class EmployeeBank{
  id!: number;
  bank_name!: string;
  bank_id!: number;
  bank_account_name!: string;
  bank_account_number!: string;
  bank_branch!: string;
}
