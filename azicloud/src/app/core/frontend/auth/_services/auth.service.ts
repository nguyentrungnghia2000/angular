import { HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { concatMap, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { Store } from 'src/app/_share/abstract/store';
import { API } from 'src/app/_share/api/api.core.config';
import { HttpService } from 'src/app/_share/services/http.service';

import { User, userLogin, userRegister } from '../_models/user';
import firebase from 'firebase/app';
import 'firebase/auth';


export class UserState {
    currentUser: User
}


@Injectable({
    providedIn: 'root'
})

export class AuthService extends Store<UserState> implements OnInit {

    readonly isLoggedIn$ = this.state$.pipe(
        map((state: UserState) => {
            return !!state.currentUser;
        })
    );


    verificationCode: string;
    expire!: Date;

    windowRef: any;
    token = localStorage.getItem('access_token');

    constructor(private http: HttpService, private router: Router) {
        super(new UserState());
    }

    ngOnInit() {

    }
    sendCode(num) {
        const appVerifier = this.windowRef.recaptchaVerifier;

        firebase.auth().signInWithPhoneNumber(JSON.stringify(num), appVerifier)
            .then((confirmationResult) => {

                this.windowRef.confirmationResult = confirmationResult;
                // ...
            }).catch((error) => {
                console.log(error)
            });
    }

    verify(id: number, verify_method_key: string) {
        return this.http.sendToServer('POST', API.USER.VERIFY, { id, verify_method_key }).pipe(shareReplay())
    }

    signup(name: string, phone: string, email: string, password: string, menthod: string, menthod_uid: string, menthod_username: string) {
        return this.http.sendToServer('POST', API.USER.REGISTER, {
            name,
            phone,
            email,
            password,
            menthod,
            menthod_uid,
            menthod_username
        }).pipe(shareReplay())
    }


    loginCreate(browser_id: number, refresh_token: string, browser_uid: string, login_ip: string, screen_width: number, screen_height: number, token_firebase: string) {
        return this.http.sendToServer('POST', API.USER.LOGIN_CREATE, {
            browser_id,
            refresh_token,
            browser_uid,
            login_ip,
            screen_width,
            screen_height,
            token_firebase
        }, { 'Authorization': `Bearer ${this.token}` });
    }

    logout() {
        localStorage.clear();
        this.router.navigate([''])
        return this.http.sendToServer('POST', API.USER.LOGOUT).pipe(shareReplay());
    }


    getAccessToken() {
        return localStorage.getItem('access_token');
    }

    getRefreshToken() {
        return localStorage.getItem('refresh_token');
    }

    setAccessToken(accessToken: string) {
        localStorage.setItem('access_token', accessToken);
    }

    setRefreshToken(refresh: string) {
        localStorage.setItem('refresh_token', refresh);
    }

    refreshToken(refresh_token:string, browser_uid:string){
      return  this.http.sendToServer('POST', API.USER.REFRESH_TOKEN, {
            refresh_token,
            browser_uid
        }).pipe(tap((res: any) => {
          if (res.status === 1) {
            console.log('get new token success');
            this.setAccessToken(res.access_token);
            this.setRefreshToken(res.refresh_token);
          } else {
            console.log('get new token unsuccess');
            this.logout();
          }
        }))
    }

    getNewAccessToken(refresh_token) {
      return this.loginCreate(0, refresh_token, "string", "string", 0, 0, "string").pipe(
        concatMap((res: any) => this.refreshToken(refresh_token, "string"))
      ).subscribe((res: any) => {
        if(res !== null){
          console.log(res);
        } else {
          console.log('fail');
        }
      });
    }

}

