import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MenuLeftComponent } from './menu-left/menu-left.component';

@NgModule({
    declarations:[MenuLeftComponent],
    imports:[
        RouterModule
    ],
    exports:[
        MenuLeftComponent
    ]

})

export class ShareModule {}