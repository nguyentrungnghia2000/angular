import { Component, DoCheck, Inject, OnInit, Renderer2 } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { Permanent } from '../../../_models/address';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';
import { ContactEntryService } from '../../../_services/contact-entry.service';
import { Country } from '../../../_models/dropdown/country';
import { map } from 'rxjs/internal/operators/map';
import { concatMap, tap } from 'rxjs/operators';
import { LoaderService } from 'src/app/_share/services/loader.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit{
  id_employee!: any;
  userId!: any;
  permanents:Permanent;
  permanentsUpdate:Permanent;

  country_list: Country[] =[];
  contry: Country;
  area_list: Country[] =[];
  area: Country;
  territory_list: Country[] =[];
  territory: Country;
  subterritory_list: Country[] =[];
  subterritory: Country;

  area_id:number;
  subterritory_id:number;
  country_id:number;
  territory_id:number;

  old_area_id:number;
  old_subterritory_id:number;
  old_country_id:number;
  old_territory_id:number;
  loadData = false;

  loading$ = this.loaderService.isLoading$;
  countryChange = false;
  areaChange = false;
  territoryChange = false;
  subterritoryChange = false;

  constructor(
    private primengConfig: PrimeNGConfig,
    private contactEntryService: ContactEntryService,
    public loaderService: LoaderService,
    private toastr: ToastrService,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private doc: Document) {
      this.permanents = new Permanent();

      this.userId = localStorage.getItem('user');
      this.id_employee = parseInt(this.userId);
     }
  ngOnInit(): void {
    this.getPermanent();
    this.getCountryList();
    this.primengConfig.ripple = true;
  }

  display: boolean = false;

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  showDialog(){
    this.permanentsUpdate = new Permanent();
    this.getPermanentUpdate();
    this.updateBodyPadding();
    this.display = true;

  }

  closeDialog(){
    this.display = false;
    this.updateBodyPadding(true);
  }

  updatePermanent(){

    if(this.countryChange === false) { this.country_id = this.permanentsUpdate.permanent_country_id; }
    if(this.areaChange === false) { this.area_id = this.permanentsUpdate.permanent_area_id; }
    if(this.territoryChange === false) { this.territory_id = this.permanentsUpdate.permanent_territory_id; }
    if(this.subterritoryChange === false) { this.subterritory_id = this.permanentsUpdate.permanent_subterritory_id; }

    if(this.country_id !== undefined && this.area_id !== undefined && this.territory_id !== undefined &&
      this.subterritory_id !== undefined && this.permanentsUpdate.permanent_address !== '' &&
      this.permanentsUpdate.domiciled !== '' && this.permanentsUpdate.current_address !== ''){

      this.contactEntryService.updateAddress(this.id_employee, this.permanentsUpdate.permanent_address,
                                            this.permanentsUpdate.domiciled, this.country_id,
                                            this.area_id, this.territory_id,
                                            this.subterritory_id, this.permanentsUpdate.current_address,
                                            this.permanentsUpdate.phone,this.permanentsUpdate.mobile).subscribe((res:any) => {
        if(res.result === 1){
          this.toastr.success('Cập nhật thành công', 'THÔNG BÁO');
          this.display = false;
          this.countryChange = false;
          this.areaChange = false;
          this.territoryChange = false;
          this.subterritoryChange = false;
          this.getPermanent();
          this.closeDialog();
        } else {
          this.toastr.error('Cập nhật thất bại', 'LỖI');
        }
      });} else {
        this.toastr.error('Vui lòng kiểm tra và nhập đầy đủ thông tin', 'LỖI');
      }
  }

  getPermanent() {
    this.loadData = true;
    this.contactEntryService.getAddress(this.id_employee).subscribe((res:any) => {
      if(res !== null){
        this.permanents = res;
        this.loadData = false;
      }
    })
  }

  getPermanentUpdate() {
    this.contactEntryService.getAddress(this.id_employee).subscribe((res:any) => {
      if(res !== null){
        this.permanentsUpdate = res;
        this.loadAllListPlace(this.permanents.permanent_country_id, this.permanents.permanent_area_id, this.permanents.permanent_territory_id);
      }
    })
  }

  getCountryList(){
    this.country_list = [];
    this.contactEntryService.getCountry().pipe(map((result: any)=>
    result.map((data: any) =>{
      return this.country_list.push(this.contry = new Country(data.name, data.id));
    }))).subscribe((res: any) =>{
      if(res !== null){
      }
      else{
        this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
      }
    })
  }

  //get list area from country_id
  getAreaList(event:any):void{
    if(event !== undefined){
      this.countryChange = true;
      //reset area_list to load new list
      this.area_list = [];
      //check if new change country id then remove data behind
      this.country_id = this.country_list.find(data => data.name === event)?.id;
      if(this.country_id !== this.permanentsUpdate.permanent_country_id){
        this.permanentsUpdate.permanent_area_name = '';
        this.permanentsUpdate.permanent_area_id = 0;
        this.permanentsUpdate.permanent_territory_name = '';
        this.permanentsUpdate.permanent_territory_id = 0;
        this.permanentsUpdate.permanent_subterritory_name = '';
        this.permanentsUpdate.permanent_subterritory_id = 0;
      }
      //call api and map data
      this.contactEntryService.getArea(this.country_id).pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.area_list.push(this.area = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res === null){
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy country_id', 'LỖI');
    }
  }

  // get list territory from area_id
  getTerritoryList(event:any):void{
    if(event !== undefined){
      this.areaChange = true;
      //reset area_list to load new list
      this.territory_list = [];
      //check if new change country id then remove data behind
      this.area_id = this.area_list.find(data => data.name === event)?.id;
      if(this.area_id !== this.permanentsUpdate.permanent_area_id){
        this.permanentsUpdate.permanent_territory_name = '';
        this.permanentsUpdate.permanent_territory_id = 0;
        this.permanentsUpdate.permanent_subterritory_name = '';
        this.permanentsUpdate.permanent_subterritory_id = 0;
      }
      //call api and map data
      this.contactEntryService.getTerritory(this.area_id).pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.territory_list.push(this.territory = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res === null){
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy area_id', 'LỖI');
    }
  }

  // get list subterritory from territory_id
  getSubterritoryList(event:any):void{
    if(event !== undefined){
      this.territoryChange = true;
      //reset area_list to load new list
      this.subterritory_list = [];
      //check if new change country id then remove data behind
      this.territory_id = this.territory_list.find(data => data.name === event)?.id;
      if(this.territory_id !== this.permanentsUpdate.permanent_territory_id){
        this.permanentsUpdate.permanent_subterritory_name = '';
        this.permanentsUpdate.permanent_subterritory_id = 0;
      }
      //call api and map data
      this.contactEntryService.getSubterritory(this.territory_id).pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.subterritory_list.push(this.subterritory = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res === null){
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy territory_id', 'LỖI');
    }
  }

  //get subterritory_id
  getIdSubterritory(event:any):void{
    if(event !== undefined){
      this.subterritoryChange = true;
      for(let i = 0; i < this.subterritory_list.length; i++){
        if(this.subterritory_list[i].name === event){
          this.subterritory_id = this.subterritory_list[i].id;
        }
      }
    } else {
      this.toastr.error('Không tìm thấy subterritory_id', 'LỖI');
    }
  }

  loadAllListPlace(country_id: number, area_id: number, territory_id: number){
    this.contactEntryService.getArea(country_id).pipe(
      tap((res: any) => res.map((data: any) => {
        return this.area_list.push(this.area = new Country(data.name, data.id));
      })),
      concatMap((res: any) => this.contactEntryService.getTerritory(area_id)),
      tap((res: any) => res.map((data: any) => {
        return this.territory_list.push(this.territory = new Country(data.name, data.id));
      })),
      concatMap((res: any) => this.contactEntryService.getSubterritory(territory_id)),
      tap((res: any) => res.map((data: any) => {
        return this.subterritory_list.push(this.subterritory = new Country(data.name, data.id));
      })),
    ).subscribe((data: any) => {
      if(data !== null){
        console.log('Load thông tin thành công');
      }
    })
  }

}

