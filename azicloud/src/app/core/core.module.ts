import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { CoreRoutingModule } from "./core-routing.module";
import { CoreComponent } from "./core.component";
import { ShareModule } from "./_share/share.module";

@NgModule({
    declarations:[
      CoreComponent
    ],
    imports:[
        CommonModule,
        CoreRoutingModule,
        ShareModule
    ],
    exports:[
    ]
})

export class CoreModule {}
