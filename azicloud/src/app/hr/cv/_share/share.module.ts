import { NgModule } from "@angular/core";
import { AvatarComponent } from "./avatar/avatar.component";
import { HeadingComponent } from "./heading/heading.component";
import { TabComponent } from "./tab/tab.component";
import { HttpClientModule } from "@angular/common/http";
//primeNG
import { DialogModule } from 'primeng/dialog';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {TabMenuModule} from 'primeng/tabmenu';
import {InputSwitchModule} from 'primeng/inputswitch';
import {CalendarModule} from 'primeng/calendar';
import { ButtonModule } from "primeng/button";
import {DropdownModule} from 'primeng/dropdown';
import {EditorModule} from 'primeng/editor';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputNumberModule} from 'primeng/inputnumber';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//ng-zorro
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { AppSharedModule } from "src/app/_share/appShare.module";

@NgModule({
    declarations:[
        AvatarComponent,
        HeadingComponent,
        TabComponent
    ],
    imports:[
        BreadcrumbModule,
        TabMenuModule,
        HttpClientModule,
        DialogModule,
        EditorModule,
        CommonModule,
        InputTextModule,
        InputTextareaModule,
        InputNumberModule,
        ScrollPanelModule,
        InputSwitchModule,
        ButtonModule,
        CalendarModule,
        FormsModule,
        NzBreadCrumbModule,
        AppSharedModule
    ],
    exports:[
        AvatarComponent,
        HeadingComponent,
        TabComponent,
    ]
})

export class ShareModule {

}
