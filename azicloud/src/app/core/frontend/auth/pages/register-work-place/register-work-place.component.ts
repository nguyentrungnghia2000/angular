import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../../_services/login.service';

@Component({
  selector: 'app-register-work-place',
  templateUrl: './register-work-place.component.html',
  styleUrls: ['./register-work-place.component.scss']
})
export class RegisterWorkPlaceComponent implements OnInit {

  name: string = '';
  address: string = '';
  url: string = '';

  constructor(private router: Router, private loginService: LoginService, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onRegis(){
    let user_id = JSON.parse(localStorage.getItem('user_id'));
    if(this.name !== '' && this.address !== '' && this.url !== ''){
      this.loginService.createCompany(user_id, this.name, this.address, this.url).subscribe((res: any) =>{
        if(res.result === 1){
          console.log(res);
          localStorage.removeItem('user_id');
          this.toastr.success('Tạo công ty thành công', 'THÔNG BÁO');
          this.router.navigate(['/core/frontend/auth/login']);
        } else{
          this.toastr.error('Tạo công ty thất bại', 'LỖI');
        }
      })
    } else {
      this.toastr.error('Vui lòng nhập đầy đủ thông tin trước khi đăng ký', 'LỖI');
    }

  }

}
