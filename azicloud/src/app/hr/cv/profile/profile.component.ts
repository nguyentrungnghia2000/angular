import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/_share/services/loader.service';

@Component({
  selector: 'app-hr',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  loading$ = this.loaderService.isLoading$;

  constructor(public loaderService: LoaderService) { }

  ngOnInit(): void {
  }

}
