import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterWorkPlaceComponent } from './register-work-place.component';

describe('RegisterWorkPlaceComponent', () => {
  let component: RegisterWorkPlaceComponent;
  let fixture: ComponentFixture<RegisterWorkPlaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterWorkPlaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterWorkPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
