export class Bank{
  id!: number;
  name!: string;
  trading_name!: string;

  constructor(id: number, name: string, trading_name: string){
    this.id = id;
    this.name = name;
    this.trading_name = trading_name;
  }
}
