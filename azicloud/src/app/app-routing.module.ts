
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_share/guards/auth.guard';

const routes: Routes = [
 { path:'hr', loadChildren: () => import('./hr/hr.module').then( m => m.HrModule)},
 { path:'core', loadChildren: () => import('./core/core.module').then( m => m.CoreModule) },
 { path:'', pathMatch:'full', redirectTo:'core' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
