import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NewPasswordService } from '../../_services/new-password.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  form: FormGroup;
  username: string = '';
  newpassword: string = '';
  newpassword_a: string = '';
  token_timeout = '';

  constructor(
    private newPasswordService: NewPasswordService,
    private router: Router,
    private toastr: ToastrService) {
      this.username = localStorage.getItem('username');
    }

  ngOnInit(): void {
  }

  onSubmit(): void {}

  onClick(): void {
    this.token_timeout = localStorage.getItem('token_timeout');
    if(this.newpassword_a === this.newpassword){
      this.newPasswordService.changePassword(this.username, this.newpassword, this.token_timeout).subscribe((data: any) => {
        if (data.result === 1) {
          this.newpassword = '';
          localStorage.removeItem('username');
          localStorage.removeItem('token_timeout');
          this.toastr.success('Đổi mật khẩu thành công', 'THÔNG BÁO');
          this.router.navigate(['/core/frontend/auth/login']);
        } else{
            if(data.result === -1){
              this.toastr.error('Email không tồn tại', 'LỖI');
            } else if(data.result === -2){
              this.toastr.warning('Token không hợp lệ. Xin hãy nhập lại', 'CẢNH BÁO');
            } else if(data.result === -3){
              this.toastr.error('Token hết hạn', 'LỖI');
              this.router.navigate(['/core/frontend/auth/login']);
            } else if(data.result === -4){
              this.toastr.error('Tài khoảng đã bị khóa. Hãy đăng nhập bằng một tài khoản khác', 'LỖI');
              this.router.navigate(['/core/frontend/auth/login']);
            }
        }
      });
    } else{
      this.toastr.error('Xác nhận mật khẩu không chính xác. Vui lòng nhập lại', 'LỖI');
    }

  }
}
