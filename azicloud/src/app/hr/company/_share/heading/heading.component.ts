import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.scss']
})
export class HeadingComponent implements OnInit {
  searchText: string = '';
  thisUrl: string = '';
  linkSearch: string = '';

  constructor(private router: Router) {
    this.thisUrl = this.router.url.toString();
  }

  ngOnInit(): void {
  }

  searchEmployeeName(){
    if(this.searchText !== ''){
      // let text = `?search=${this.searchText}`;
      this.router.navigate(['/hr/company/employee/main', {text: this.searchText}]);
    } else{
      this.router.navigate(['/hr/company/employee/main']);
    }
  }

  textChange(event){
    if(event.target.value !== ''){
      this.searchText = event.target.value;
    } else{
      this.searchText = '';
    }
  }
}
