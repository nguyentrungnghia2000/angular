
export class DropdownItem{
    id: string;
    name: string;
    constructor(id, name : string){
        this.id = id;
        this.name =name;
    }
}