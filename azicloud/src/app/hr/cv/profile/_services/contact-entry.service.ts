import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, map } from 'rxjs/operators';
import { HttpService } from 'src/app/_share/services/http.service';
import { API } from 'src/app/_share/api/api.hr.config';

@Injectable({
  providedIn: 'root'
})
export class ContactEntryService {
  token: string ='';

  constructor(private http: HttpService) {
    this.token = localStorage.getItem('access_token');
  }

  getListDependent(id){
    return this.http.sendToServer('GET',API.EMPLOYEE.DEPENDENT_LIST(id),null,{'Authorization':`Bearer ${this.token}`});
  }

  CreateDependent(id_employee, id, country_id, area_id, territory_id, subterritory_id, name, email,
                  relationship_key, birth_day, birth_month, birth_year, gender_key, identity_card, tax_code, start_date, end_date){
    return this.http.sendToServer('POST',API.EMPLOYEE.DEPENDENT_CREATE(id_employee),{
      id, country_id,
      area_id, territory_id,
      subterritory_id, name,
      email, relationship_key,
      birth_day, birth_month,
      birth_year, gender_key,
      identity_card, tax_code,
      start_date, end_date},{'Authorization':`Bearer ${this.token}`});
  }

  deleteDenpendent(id){
    return this.http.sendToServer('POST',API.EMPLOYEE.DEPENDENT_DELETE(id),null,{'Authorization':`Bearer ${this.token}`});
  }

  getCountry(){
    return this.http.sendToServer('GET',API.EMPLOYEE.COUNTRY_LIST,null,{'Authorization': `Bearer ${this.token}`});

  }
  getArea(id){
    return this.http.sendToServer('GET',API.EMPLOYEE.AREA_LIST(id),null,{'Authorization': `Bearer ${this.token}`});

  }
  getTerritory(id){
    return this.http.sendToServer('GET',API.EMPLOYEE.TERRITORY_LIST(id),null,{'Authorization': `Bearer ${this.token}`});

  }
  getSubterritory(id){
    return this.http.sendToServer('GET',API.EMPLOYEE.SUBTERRITORY_LIST(id),null,{'Authorization': `Bearer ${this.token}`});

  }
  getViewDependent(id){

    return this.http.sendToServer('GET',API.EMPLOYEE.DEPENDENT_VIEW(id),null,{'Authorization': `Bearer ${this.token}`});
  }

  getAddress(id){
    return this.http.sendToServer('GET',API.EMPLOYEE.PERMANENT_ADDRESS_VIEW(id),null,{'Authorization':`Bearer ${this.token}`});
  }

  updateAddress(id_employee, permanent_address, domiciled, permanent_country_id,
                permanent_area_id, permanent_territory_id, permanent_subterritory_id, current_address,phone,mobile){
    return this.http.sendToServer('POST',API.EMPLOYEE.PERMANENT_ADDRESS_UPDATE(id_employee),{
      permanent_address, domiciled,
      permanent_country_id,
      permanent_area_id,
      permanent_territory_id,
      permanent_subterritory_id,
      current_address,
      phone,
      mobile},{'Authorization':`Bearer ${this.token}`});
  }

  getCurrentPlace(id){
    return this.http.sendToServer('GET',API.EMPLOYEE.ADDRESS_VIEW(id),null,{'Authorization':`Bearer ${this.token}`});
  }

  updateCurrenPlace(id_employee, current_address, phone, mobile, current_country_id, current_area_id, current_territory_id, current_subterritory_id){
    return this.http.sendToServer('POST',API.EMPLOYEE.ADDRESS_UPDATE(id_employee),{
      current_address,
      phone, mobile,
      current_country_id,
      current_area_id,
      current_territory_id,
      current_subterritory_id},{'Authorization':`Bearer ${this.token}`});
  }

  getEmergence(id){
    return this.http.sendToServer('GET',API.EMPLOYEE.EMERGENCY_VIEW(id),null,{'Authorization':`Bearer ${this.token}`});
  }

  updateEmergency(id, emergency_address,name,emergency_relationship,emergency_mobile,emergency_phone){
    return this.http.sendToServer('POST',API.EMPLOYEE.EMERGENCY_UPDATE(id),{
      emergency_address,
      name,
      emergency_relationship,
      emergency_mobile,
      emergency_phone},{'Authorization':`Bearer ${this.token}`});
  }
}
