import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeEntryContactComponent } from './employee-entry-contact.component';

describe('EmployeeEntryContactComponent', () => {
  let component: EmployeeEntryContactComponent;
  let fixture: ComponentFixture<EmployeeEntryContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeEntryContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeEntryContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
