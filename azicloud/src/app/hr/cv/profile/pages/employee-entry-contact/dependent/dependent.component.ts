import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { InfoEntryService } from '../../../_services/info-entry.service';
import { PrimeNGConfig } from 'primeng/api';
import { Dependent_list,Dependent } from '../../../_models/address';
import { Configs } from '../../../_models/dropdown/config';
import { concatMap, map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';
import { ContactEntryService } from '../../../_services/contact-entry.service';
import { Country } from '../../../_models/dropdown/country';
import { LoaderService } from 'src/app/_share/services/loader.service';

@Component({
  selector: 'app-dependent',
  templateUrl: './dependent.component.html',
  styleUrls: ['./dependent.component.scss']
})
export class DependentComponent implements OnInit {
  id_employee!: any;
  userId!: any;
  id:number;
  country_id:number;
  area_id:number;
  territory_id:number;
  subterritory_id:number;
  loadData = false;
  birthday: Date;
  fakeBirthday: Date;

  loadedAreaList = false;
  loadedTerritoryList = false;
  loadedSubterritoryList = false;
  startDateChecked = false;
  endDateChecked = false;

  country_list: Country[] =[];
  contry: Country;
  area_list: Country[] =[];
  area: Country;
  territory_list: Country[] =[];
  territory: Country;
  subterritory_list: Country[] =[];
  subterritory: Country;
  public dependent:Dependent;
  public dependentUpdate:Dependent;
  public dependents_list:Dependent_list[]=[]
  ListGenders: Configs[] = [];
  gender: Configs;
  iddelete:number

  countryChange = false;
  areaChange = false;
  territoryChange = false;
  subterritoryChange = false;
  loading$ = this.loaderService.isLoading$;

  constructor(
              private primengConfig: PrimeNGConfig,
              private infoEntryService: InfoEntryService,
              private toastr: ToastrService,
              public loaderService: LoaderService,
              private renderer: Renderer2,
              private contactEntryService: ContactEntryService,
              @Inject(DOCUMENT) private doc: Document) {
      this.dependent = new Dependent();

      this.userId = localStorage.getItem('user');
      this.id_employee = parseInt(this.userId);
    }

  ngOnInit(): void {
    this.getCountryList();
    this.primengConfig.ripple = true;
    this.getListDependent();
    this.getListGenders();
  }
    displaycreate: boolean = false
    displayupdate: boolean = false
    deleteDisplay: boolean = false

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }
  showDialogCreate(){
    this.displaycreate = true;
    this.dependent = new Dependent();
    this.fakeBirthday = new Date();
    this.updateBodyPadding();
  }

  showDialogdelete(id:number){
    this.deleteDisplay =  true;
    this.iddelete = id;
    this.updateBodyPadding();
  }

  showDialogUpdate(id){

    this.dependentUpdate = new Dependent();
    this.updateBodyPadding();
    this.contactEntryService.getViewDependent(id).subscribe(async (res:any) => {
      if(res !== null){
        this.dependentUpdate = res;
        if(this.dependentUpdate.birth_year !== 0 && this.dependentUpdate.birth_month !== 0 && this.dependentUpdate.birth_day !== 0){
          this.birthday = new Date(`${this.dependentUpdate.birth_year}-${this.dependentUpdate.birth_month}-${this.dependentUpdate.birth_day}`);
        } else{
          this.birthday = new Date();
        }
        this.loadAllListPlace(this.dependentUpdate.country_id, this.dependentUpdate.area_id, this.dependentUpdate.territory_id);
      }
    });
    this.displayupdate = true;
  }

  closeDialog(){
    this.displayupdate = false;
    this.displaycreate = false;
    this.deleteDisplay = false;
    this.updateBodyPadding(true);
  }

  createDependent(){

    if(this.country_id !== undefined && this.area_id !== undefined &&
        this.territory_id !== undefined && this.subterritory_id !== undefined && this.dependent.name !== undefined &&
        this.dependent.email !== undefined && this.dependent.relationship_key !== undefined && this.dependent.birth_day !== undefined &&
        this.dependent.birth_month !== undefined && this.dependent.birth_year !== undefined && this.dependent.gender_key !== undefined &&
        this.dependent.identity_card !== undefined && this.dependent.tax_code !== undefined && this.dependent.start_date !== undefined &&
        this.dependent.end_date !== undefined){

      this.contactEntryService.CreateDependent(this.id_employee, -1, this.country_id, this.area_id,
                                                this.territory_id, this.subterritory_id, this.dependent.name,
                                                this.dependent.email, this.dependent.relationship_key, this.dependent.birth_day, this.dependent.birth_month,
                                                this.dependent.birth_year, this.dependent.gender_key, this.dependent.identity_card, this.dependent.tax_code,
                                                this.dependent.start_date, this.dependent.end_date).subscribe((res:any) => {
          if(res.result === 1){
            this.dependents_list=[];
            this.displaycreate = false;
            this.getListDependent()
            this.toastr.success('Tạo mới thành công', 'THÔNG BÁO');
            this.closeDialog();
          }
          else{
            this.toastr.error('Tạo mới thất bại', 'LỖI');
          }
        });
    } else {
      this.toastr.warning('Chưa nhập đầy đủ thông tin. Vui lòng kiểm tra lại', 'CẢNH BÁO');
    }
  }

  getListDependent(){
    this.loadData = true;
    this.contactEntryService.getListDependent(this.id_employee).subscribe((res:any) => {
      if(res !== null){
        this.dependents_list = res;
        this.loadData = false;
      }
    });
  }

  deleteDependent(){
    this.deleteDisplay =  false;
    this.contactEntryService.deleteDenpendent(this.iddelete).subscribe((res:any) => {
      if(res.result === 1){
        this.dependents_list=[];
        this.getListDependent()
        this.toastr.success('Xóa thành công', 'THÔNG BÁO');
        this.closeDialog();
      } else {
        this.toastr.error('Xóa thất bại', 'LỖI');
      }
    })
  }

  updateDependent(){

    if(!this.startDateChecked || !this.endDateChecked){
      if(!this.startDateChecked){
        let d = new Date(Date.parse(this.dependentUpdate.start_date));
        this.dependentUpdate.start_date = d.toISOString();
      } else{
        let d = new Date(Date.parse(this.dependentUpdate.end_date));
        this.dependentUpdate.end_date = d.toISOString();
      }
    }
    if(this.countryChange === false) { this.country_id = this.dependentUpdate.country_id; }
    if(this.areaChange === false) { this.area_id = this.dependentUpdate.area_id; }
    if(this.territoryChange === false) { this.territory_id = this.dependentUpdate.territory_id; }
    if(this.subterritoryChange === false) { this.subterritory_id = this.dependentUpdate.subterritory_id; }

    if(this.country_id !== undefined && this.area_id !== undefined &&
      this.territory_id !== undefined && this.subterritory_id !== undefined && this.dependentUpdate.name !== '' &&
      this.dependentUpdate.email !== '' && this.dependentUpdate.relationship_key !== undefined && this.dependentUpdate.birth_day !== undefined &&
      this.dependentUpdate.birth_month !== undefined && this.dependentUpdate.birth_year !== undefined && this.dependentUpdate.gender_key !== undefined &&
      this.dependentUpdate.identity_card !== '' && this.dependentUpdate.tax_code !== '' && this.dependentUpdate.start_date !== undefined &&
      this.dependentUpdate.end_date !== undefined){
    this.contactEntryService.CreateDependent(this.id_employee, this.dependentUpdate.id, this.country_id, this.area_id,
                                            this.territory_id, this.subterritory_id, this.dependentUpdate.name,
                                            this.dependentUpdate.email, this.dependentUpdate.relationship_key, this.dependentUpdate.birth_day, this.dependentUpdate.birth_month,
                                            this.dependentUpdate.birth_year, this.dependentUpdate.gender_key, this.dependentUpdate.identity_card, this.dependentUpdate.tax_code,
                                            this.dependentUpdate.start_date, this.dependentUpdate.end_date).subscribe((res: any) => {
      if(res.result === 2){
        if(this.startDateChecked || this.endDateChecked){
          if(this.startDateChecked){
            this.startDateChecked = false;
          } else{
            this.endDateChecked = false;
          }
        }
        this.dependents_list=[];
        this.toastr.success('Cập nhật thành công', 'THÔNG BÁO');
        this.displayupdate = false;
        this.countryChange = false;
        this.areaChange = false;
        this.territoryChange = false;
        this.subterritoryChange = false;
        this.getListDependent();
        this.updateBodyPadding();
        this.closeDialog();
      } else{
        this.toastr.error('Cập nhật thất bại', 'LỖI');
      }
    })} else {
      this.toastr.error('Vui lòng kiểm tra lại thông tin nhập', 'LỖI');
    }
  }

  getCountryList(){
    this.country_list = [];
    this.contactEntryService.getCountry().pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.country_list.push(this.contry = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res !== null){
        }
        else{
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
    })
  }

  //get list area from country_id
  getAreaList(event:any):void{
    if(event !== undefined){
      this.countryChange = true;
      //reset area_list to load new list
      this.area_list = [];
      //check if new change country id then remove data behind
      this.country_id = this.country_list.find(data => data.name === event)?.id;
      if(this.country_id !== this.dependentUpdate.country_id){
        this.dependentUpdate.area_name = '';
        this.dependentUpdate.area_id = 0;
        this.dependentUpdate.territory_name = '';
        this.dependentUpdate.territory_id = 0;
        this.dependentUpdate.subterritory_name = '';
        this.dependentUpdate.subterritory_id = 0;
      }

      this.dependent.area_name = '';
      this.dependent.area_id = 0;
      this.dependent.territory_name = '';
      this.dependent.territory_id = 0;
      this.dependent.subterritory_name = '';
      this.dependent.subterritory_id = 0;
      //call api and map data
      this.contactEntryService.getArea(this.country_id).pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.area_list.push(this.area = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res === null){
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy country_id', 'LỖI');
    }
  }

  // get list territory from area_id
  getTerritoryList(event:any):void{
    if(event !== undefined){
      this.areaChange = true;
      //reset area_list to load new list
      this.territory_list = [];
      //check if new change country id then remove data behind
      this.area_id = this.area_list.find(data => data.name === event)?.id;
      if(this.area_id !== this.dependentUpdate.area_id){
        this.dependentUpdate.territory_name = '';
        this.dependentUpdate.territory_id = 0;
        this.dependentUpdate.subterritory_name = '';
        this.dependentUpdate.subterritory_id = 0;
      }

      this.dependent.territory_name = '';
      this.dependent.territory_id = 0;
      this.dependent.subterritory_name = '';
      this.dependent.subterritory_id = 0;
      //call api and map data
      this.contactEntryService.getTerritory(this.area_id).pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.territory_list.push(this.territory = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res === null){
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy area_id', 'LỖI');
    }
  }

  // get list subterritory from territory_id
  getSubterritoryList(event:any):void{
    if(event !== undefined){
      this.territoryChange = true;
      //reset area_list to load new list
      this.subterritory_list = [];
      //check if new change country id then remove data behind
      this.territory_id = this.territory_list.find(data => data.name === event)?.id;
      if(this.territory_id !== this.dependentUpdate.territory_id){
        this.dependentUpdate.subterritory_name = '';
        this.dependentUpdate.subterritory_id = 0;
      }

      this.dependent.subterritory_name = '';
      this.dependent.subterritory_id = 0;
      //call api and map data
      this.contactEntryService.getSubterritory(this.territory_id).pipe(map((result: any)=>
      result.map((data: any) =>{
        return this.subterritory_list.push(this.subterritory = new Country(data.name, data.id));
      }))).subscribe((res: any) =>{
        if(res === null){
          this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy territory_id', 'LỖI');
    }
  }

  //get subterritory_id
  getIdSubterritory(event:any):void{
    if(event !== undefined){
      this.subterritoryChange = true;
      this.subterritory_id = this.subterritory_list.find(data => data.name === event)?.id;
    } else {
      this.toastr.error('Không tìm thấy subterritory_id', 'LỖI');
    }
  }

  //split birthday to day, month, year
  onSelected(event){
    let d = new Date(Date.parse(event));
    let strDate = d.toString();
    let arrDate: string[] = strDate.split(" ");
    this.dependent.birth_day = parseInt(arrDate[2]);
    this.dependent.birth_year = parseInt(arrDate[3]);
    switch(arrDate[1]){
      case "Jan":
        this.dependent.birth_month = 1
        break;
      case "Feb":
        this.dependent.birth_month = 2
        break;
      case "Mar":
        this.dependent.birth_month = 3
        break;
      case "Apr":
        this.dependent.birth_month = 4
        break;
      case "May":
        this.dependent.birth_month = 5
        break;
      case "Jun":
        this.dependent.birth_month = 6
        break;
      case "Jul":
        this.dependent.birth_month = 7
        break;
      case "Aug":
        this.dependent.birth_month = 8
        break;
      case "Sep":
        this.dependent.birth_month = 9
        break;
      case "Oct":
        this.dependent.birth_month = 10
        break;
      case "Nov":
        this.dependent.birth_month = 11
        break;
      case "Dec":
        this.dependent.birth_month = 12
        break;
    }
  }

  //split birthday to day, month, year
  onSelectedUpdate(event){
    let d = new Date(Date.parse(event));
    let strDate = d.toString();
    let arrDate: string[] = strDate.split(" ");
    this.dependentUpdate.birth_day = parseInt(arrDate[2]);
    this.dependentUpdate.birth_year = parseInt(arrDate[3]);
    switch(arrDate[1]){
      case "Jan":
        this.dependentUpdate.birth_month = 1
        break;
      case "Feb":
        this.dependentUpdate.birth_month = 2
        break;
      case "Mar":
        this.dependentUpdate.birth_month = 3
        break;
      case "Apr":
        this.dependentUpdate.birth_month = 4
        break;
      case "May":
        this.dependentUpdate.birth_month = 5
        break;
      case "Jun":
        this.dependentUpdate.birth_month = 6
        break;
      case "Jul":
        this.dependentUpdate.birth_month = 7
        break;
      case "Aug":
        this.dependentUpdate.birth_month = 8
        break;
      case "Sep":
        this.dependentUpdate.birth_month = 9
        break;
      case "Oct":
        this.dependentUpdate.birth_month = 10
        break;
      case "Nov":
        this.dependentUpdate.birth_month = 11
        break;
      case "Dec":
        this.dependentUpdate.birth_month = 12
        break;
    }
  }

  //get list genders for combobox
  getListGenders(){
    this.infoEntryService.getListGenders().pipe(map((result: any)=>
    result.map((data: any) =>{
      return this.ListGenders.push(this.gender = new Configs(data.value, data.key));
    }))).subscribe((res: any) =>{
      if(res !== null){
      }
    });
  }

  //get gender.key from gender.value
  onChangeGender(event){
    this.dependent.gender_key = this.ListGenders.find(data => data.value === event)?.key;
  }
  onChangeGenderUpdate(event){
    this.dependentUpdate.gender_key = this.ListGenders.find(data => data.value === event)?.key;
  }

  //convert start date to JSON string
  onSelectedStartDateUpdate(event){
    let d = new Date(Date.parse(event));
    this.dependentUpdate.start_date = d.toISOString();
    this.startDateChecked = true;
  }

  onSelectedEndDateUpdate(event){
    let d = new Date(Date.parse(event));
    this.dependentUpdate.end_date = d.toISOString();
    this.endDateChecked = true;
  }

  onSelectedStartDateCreate(event){
    let d = new Date(Date.parse(event));
    this.dependent.start_date = d.toISOString();
    this.startDateChecked = true;
    console.log(this.dependent.start_date)
  }

  onSelectedEndDateCreate(event){
    let d = new Date(Date.parse(event));
    this.dependent.end_date = d.toISOString();
    this.endDateChecked = true;
  }

  loadAllListPlace(country_id: number, area_id: number, territory_id: number){
    this.contactEntryService.getArea(country_id).pipe(
      tap((res: any) => res.map((data: any) => {
        return this.area_list.push(this.area = new Country(data.name, data.id));
      })),
      concatMap((res: any) => this.contactEntryService.getTerritory(area_id)),
      tap((res: any) => res.map((data: any) => {
        return this.territory_list.push(this.territory = new Country(data.name, data.id));
      })),
      concatMap((res: any) => this.contactEntryService.getSubterritory(territory_id)),
      tap((res: any) => res.map((data: any) => {
        return this.subterritory_list.push(this.subterritory = new Country(data.name, data.id));
      })),
    ).subscribe((data: any) => {
      if(data !== null){
        console.log('Load thông tin thành công');
      }
    })
  }
}
