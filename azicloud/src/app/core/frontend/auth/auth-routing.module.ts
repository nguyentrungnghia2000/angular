import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthComponent } from "./auth.component";
import { ChangePasswordComponent } from "./pages/change-password/change-password.component";
import { ForgotPasswordComponent } from "./pages/forgot-password/forgot-password.component";
import { LoginComponent } from "./pages/login/login.component";
import { OptComponent } from "./pages/otp/opt.component";
import { RegisterComponent } from "./pages/register/register.component";
import { CommonModule } from "@angular/common";
import { VerifyTokenTimeoutComponent } from "./pages/verify-token-timeout/verify-token-timeout.component";
import { RegisterWorkPlaceComponent } from "./pages/register-work-place/register-work-place.component";
import { LinkComponent } from "./pages/link/link.component";
import { SecurityQuestionComponent } from "./pages/security-question/security-question.component";
import { ChoosePlaceComponent } from "./pages/choose-place/choose-place.component";
import { LinkOtpComponent } from "./pages/link-otp/link-otp.component";

const routes: Routes =[
    { path:'', component: AuthComponent,
      children:[
        { path: 'login', component: LoginComponent},
        { path: 'signup', component: RegisterComponent},
        { path: 'forgotpassword', component: ForgotPasswordComponent},
        { path: 'tokentimeout', component: VerifyTokenTimeoutComponent},
        { path: 'change', component: ChangePasswordComponent},
        { path: 'otp', component: OptComponent},
        { path: 'regisworkplace', component: RegisterWorkPlaceComponent},
        { path: 'link', component: LinkComponent},
        { path: 'security', component: SecurityQuestionComponent},
        { path: 'choose', component: ChoosePlaceComponent},
        { path: 'link-otp', component: LinkOtpComponent},
        { path:'', pathMatch:'full', redirectTo:'login' }
    ]}
]
@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class AuthRoutingModule {}
