import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CvComponent } from "./cv.component";


const routes: Routes = [
    { path:'', component:CvComponent, children: [
            { path:'profile', loadChildren:() => import('./profile/profile.module').then(m => m.ProfileModule) },
            { path:'', pathMatch:'full',redirectTo:'profile' }
        ]
    }
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class CvRoutingModule {}
