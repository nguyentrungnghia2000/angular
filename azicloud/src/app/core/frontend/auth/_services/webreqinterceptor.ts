import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Observable, throwError,  Subject, BehaviorSubject } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, tap, switchMap, filter, take, concatMap, debounceTime } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class WebReqInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private cookieService: CookieService) { }

  private  isRefreshing: boolean = false;
  private  isRefreshingCreate: boolean = false;

  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  refreshToken: string;
  access_token: string;
  browser_uid: string;

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<Object>> {

    request = this.addToken(request, this.authService.getAccessToken());


    return next.handle(request).pipe(catchError((error:HttpErrorResponse )=> {

      if(error.status === 401){
        return this.handle401Error(request, next);
      }
      return throwError(error);
    })) ;
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);
      this.refreshToken = localStorage.getItem('refresh_token');
      this.browser_uid = this.cookieService.get('browser_uid');
      console.log(this.browser_uid);
      if(this.refreshToken){
        return this.authService.refreshToken(this.refreshToken, this.browser_uid).pipe(
          switchMap((res: any) => {
            this.isRefreshing = false;
            this.refreshTokenSubject.next(res.access_token);
            return next.handle(this.addToken(request, res.access_token));
          }));
      }
    } else {
      return this.refreshTokenSubject.pipe(
        filter(token => token != null),
        take(1),
        switchMap(jwt => {
          return next.handle(this.addToken(request, jwt));
        }));
    }
  }

  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        'Authorization': `Bearer ${token}`
      }
    });
  }
}
