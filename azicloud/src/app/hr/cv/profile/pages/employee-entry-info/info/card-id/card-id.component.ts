import { DOCUMENT } from '@angular/common';
import { ThisReceiver } from '@angular/compiler';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { tap, concatMap } from 'rxjs/operators';
import { Company } from 'src/app/core/frontend/auth/_models/company.module';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { CardID } from '../../../../_models/employee-info-entry/employee-cardid';
import { ImageUpLoad } from '../../../../_models/image.model';
import { InfoEntryService } from '../../../../_services/info-entry.service';

@Component({
  selector: 'app-card-id',
  templateUrl: './card-id.component.html',
  styleUrls: ['./card-id.component.scss']
})
export class CardIdComponent implements OnInit {

  display: boolean = false;
  loadData = false;
  frontOk = false;
  backOk = false;
  userId!: any;
  updateDateChecked = false;
  employeeCardId: CardID;
  employeeCardIdUpdate: CardID;
  frontImageUpload: ImageUpLoad;
  backImageUpload: ImageUpLoad;
  frontUrl: any;
  backUrl: any;
  frontImage!: File;
  backImage: File = null;
  frontImageLoad: string;
  backImageLoad: string;

  identityCardDate: Date;
  userCompany: Company;
  uid  = 0;

  loading$ = this.loaderService.isLoading$;

  constructor(private infoEntryService: InfoEntryService,
              private toastr: ToastrService,
              public loaderService: LoaderService,
              private renderer: Renderer2,
              @Inject(DOCUMENT) private doc: Document) {
    this.userId = localStorage.getItem('user');
    this.uid = parseInt(this.userId);
    this.userCompany = new Company();
    this.userCompany = JSON.parse(localStorage.getItem('usercompany'));
    this.employeeCardId = new CardID();
    this.frontImageUpload = new ImageUpLoad();
    this.backImageUpload = new ImageUpLoad();
   }

  ngOnInit(): void {
    this.LoadEmployeeCardId();
  }

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  showDialog() {
    this.frontUrl = `http://api.azicloud.vn/${this.employeeCardId.identity_front_image_filepath}${this.employeeCardId.identity_front_image_filename}`;
    this.backUrl = `http://api.azicloud.vn/${this.employeeCardId.identity_backside_image_filepath}${this.employeeCardId.identity_backside_image_filename}`;
    this.employeeCardIdUpdate = new CardID();
    this.LoadEmployeeCardIdUpdate();
    this.display = true;
    this.updateBodyPadding();
  }

  closeDialog(){
    this.display = false;
    this.frontUrl = null;
    this.backUrl = null;
    this.updateBodyPadding(true);
  }

  LoadEmployeeCardId(){
    this.loadData = true;
    this.infoEntryService.getEmployeeCardID(this.uid).subscribe((res: any) => {
      if(res !== null){
        this.loadImageID(res);
        this.loadData = false;
      }
      else{
        this.toastr.error('Không thể tải về thông tin chứng minh nhân dân', 'LỖI');
      }
    });
  }

  LoadEmployeeCardIdUpdate(){
    this.infoEntryService.getEmployeeCardID(this.uid).subscribe((res: any) => {
      if(res !== null){
        this.employeeCardIdUpdate = res;
        this.identityCardDate = new Date(this.employeeCardIdUpdate.identity_card_date);
      }
      else{
        this.toastr.error('Không thể tải về thông tin chứng minh nhân dân', 'LỖI');
      }
    });
  }

  //load identity image
  loadImageID(res: any){
    this.employeeCardId = res;
    this.employeeCardId.identity_card_date = this.getDateOnly(this.employeeCardId.identity_card_date);
    this.frontImageLoad = `http://api.azicloud.vn/${this.employeeCardId.identity_front_image_filepath}${this.employeeCardId.identity_front_image_filename}`;
    this.backImageLoad = `http://api.azicloud.vn/${this.employeeCardId.identity_backside_image_filepath}${this.employeeCardId.identity_backside_image_filename}`;
  }

  onUpdate(){

    //automatic convert date to JSON-DATE if user dont update date
    if(!this.updateDateChecked){
      let d = new Date(Date.parse(this.employeeCardId.identity_card_date));
      this.employeeCardIdUpdate.identity_card_date = d.toISOString();
    }

    //conditions for image (if there are image imported or not)
    if(this.frontOk === false && this.backOk === false){
      this.updateEmployeeCardId();
    } else{
      if(this.frontOk === true && this.backOk === true){
        this.updateWithFrontAndBackImageAdded();
      }else if(this.frontOk === true && this.backOk === false){
        this.updateWithFrontImageAdded();
      }else if(this.frontOk === false && this.backOk === true){
        this.updateWithBackImageAdded();
      }
    }
  }

  //update identity card
  updateEmployeeCardId(){

    this.infoEntryService.updateEmployeeCardID(this.uid, this.employeeCardIdUpdate.identity_card, this.employeeCardIdUpdate.identity_card_date,
                                                  this.employeeCardIdUpdate.identity_card_issued, this.employeeCardIdUpdate.identity_front_image_id,
                                                  this.employeeCardIdUpdate.identity_backside_image_id).subscribe((res: any) => {
      if(res.result === 2){
        this.toastr.success('Cập nhật chứng minh nhân dân thành công', 'THÔNG BÁO');
        this.closeDialog();
        this.frontUrl = null;
        this.backUrl = null;
        this.frontOk = false;
        this.backOk = false;
        this.updateDateChecked = false;
        this.LoadEmployeeCardId();
      } else{
        this.toastr.error('Cập nhật chứng minh nhân dân thất bại', 'LỖI');
      }
    });
  }

  //convert date to JSON string
  onSelectedUpdate(event){
    let d = new Date(Date.parse(event));
    this.employeeCardIdUpdate.identity_card_date = d.toISOString();
    this.updateDateChecked = true;
  }

  //split date only to show on label
  getDateOnly(str: string){
    let arrTemp: string[] = str.split('T');
    return arrTemp[0];
  }

  //get file from uploader input
  onSelectedFrontImage(event){
    this.frontImage = <File>event.target.files[0];

    if(!event.target.files[0] || event.target.files[0].length == 0) {
			return;
		}
    let mimeType = event.target.files[0].type;
		if (mimeType.match(/image\/*/) == null) {
			this.toastr.warning('Hệ thống chỉ hỗ trợ mục hình ảnh', 'CẢNH BÁO');
			return;
		}

		let reader = new FileReader();
		reader.readAsDataURL(this.frontImage);
		reader.onload = () => {
			this.frontUrl = reader.result;
      this.frontOk = true;
		}
  }

  //get file from uploader input
  onSelectedBackImage(event){
    this.backImage = event.target.files[0];

    if(!event.target.files[0] || event.target.files[0].length == 0) {
			return;
		}
    let mimeType = event.target.files[0].type;
		if (mimeType.match(/image\/*/) == null) {
			this.toastr.warning('Hệ thống chỉ hỗ trợ mục hình ảnh', 'CẢNH BÁO');
			return;
		}

		let reader = new FileReader();
		reader.readAsDataURL(this.backImage);
		reader.onload = () => {
			this.backUrl = reader.result;
      this.backOk = true;
		}
  }

  updateWithBackImageAdded(){
    this.infoEntryService.uploadImage(this.uid, this.userCompany.url, this.backImage).pipe(
      tap((res: any) => {
        if(res.result === 1){
          this.backImageUpload = res;
          this.employeeCardIdUpdate.identity_backside_image_id = this.backImageUpload.id;
          this.backImage = null;
          this.toastr.success('Đã nhận ảnh mặt sau', 'THÔNG BÁO');
        } else{
          this.toastr.error('Chưa nhận được ảnh mặt sau', 'LỖI');
        }
      }),
      concatMap((result: any) => this.infoEntryService.updateEmployeeCardID(this.uid, this.employeeCardIdUpdate.identity_card,
                                                                            this.employeeCardIdUpdate.identity_card_date,
                                                                            this.employeeCardIdUpdate.identity_card_issued,
                                                                            this.employeeCardIdUpdate.identity_front_image_id,
                                                                            this.employeeCardIdUpdate.identity_backside_image_id)),
      tap((res: any) => {
        if(res.result === 2){
          this.toastr.success('Cập nhật chứng minh nhân dân thành công', 'THÔNG BÁO');
          this.closeDialog();
          this.frontUrl = null;
          this.backUrl = null;
          this.frontOk = false;
          this.backOk = false;
          this.updateDateChecked = false;
          this.LoadEmployeeCardId();
        } else{
          this.toastr.error('Cập nhật chứng minh nhân dân thất bại', 'LỖI');
        }
      })
    ).subscribe((data: any) => {
      console.log(data);
    })
  }

  updateWithFrontImageAdded(){
    this.infoEntryService.uploadImage(this.uid, this.userCompany.url, this.frontImage).pipe(
      tap((res: any) => {
        if(res.result === 1){
          this.frontImageUpload = res;
          this.employeeCardIdUpdate.identity_front_image_id = this.frontImageUpload.id;
          this.frontImage = null;
          this.toastr.success('Đã nhận ảnh mặt trước', 'THÔNG BÁO');
        } else{
          this.toastr.error('Chưa nhận được ảnh mặt trước', 'LỖI');
        }
      }),
      concatMap((result: any) => this.infoEntryService.updateEmployeeCardID(this.uid, this.employeeCardIdUpdate.identity_card,
                                                                            this.employeeCardIdUpdate.identity_card_date,
                                                                            this.employeeCardIdUpdate.identity_card_issued,
                                                                            this.employeeCardIdUpdate.identity_front_image_id,
                                                                            this.employeeCardIdUpdate.identity_backside_image_id)),
      tap((res: any) => {
        if(res.result === 2){
          this.toastr.success('Cập nhật chứng minh nhân dân thành công', 'THÔNG BÁO');
          this.closeDialog();
          this.frontUrl = null;
          this.backUrl = null;
          this.frontOk = false;
          this.backOk = false;
          this.updateDateChecked = false;
          this.LoadEmployeeCardId();
        } else{
          this.toastr.error('Cập nhật chứng minh nhân dân thất bại', 'LỖI');
        }
      })
    ).subscribe((data: any) => {
      console.log(data);
    })
  }

  updateWithFrontAndBackImageAdded(){
    this.infoEntryService.uploadImage(this.uid, this.userCompany.url, this.frontImage).pipe(
      tap((res: any) => {
        if(res.result === 1){
          this.frontImageUpload = res;
          this.employeeCardIdUpdate.identity_front_image_id = this.frontImageUpload.id;
          this.frontOk = true;
          this.frontImage = null;
          this.toastr.success('Đã nhận ảnh mặt trước', 'THÔNG BÁO');
        } else{
          this.toastr.error('Chưa nhận được ảnh mặt trước', 'LỖI');
        }
      }),
      concatMap((result: any) => this.infoEntryService.uploadImage(this.uid, this.userCompany.url, this.backImage)),
      tap((res: any) => {
        if(res.result === 1){
          this.backImageUpload = res;
          this.employeeCardIdUpdate.identity_backside_image_id = this.backImageUpload.id;
          this.backOk = true;
          this.backImage = null;
          this.toastr.success('Đã nhận ảnh mặt sau', 'THÔNG BÁO');
        } else{
          this.toastr.error('Chưa nhận được ảnh mặt sau', 'LỖI');
        }
      }),
      concatMap((result: any) => this.infoEntryService.updateEmployeeCardID(this.uid, this.employeeCardIdUpdate.identity_card,
                                                                            this.employeeCardIdUpdate.identity_card_date,
                                                                            this.employeeCardIdUpdate.identity_card_issued,
                                                                            this.employeeCardIdUpdate.identity_front_image_id,
                                                                            this.employeeCardIdUpdate.identity_backside_image_id)),
      tap((res: any) => {
        if(res.result === 2){
          this.toastr.success('Cập nhật chứng minh nhân dân thành công', 'THÔNG BÁO');
          this.closeDialog();
          this.frontUrl = null;
          this.backUrl = null;
          this.frontOk = false;
          this.backOk = false;
          this.updateDateChecked = false;
          this.LoadEmployeeCardId();
        } else{
          this.toastr.error('Cập nhật chứng minh nhân dân thất bại', 'LỖI');
        }
      })
    ).subscribe((data: any) => {
      console.log(data);
    })
  }

}
