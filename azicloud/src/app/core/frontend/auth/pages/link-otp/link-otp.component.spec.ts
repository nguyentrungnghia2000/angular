import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkOtpComponent } from './link-otp.component';

describe('LinkOtpComponent', () => {
  let component: LinkOtpComponent;
  let fixture: ComponentFixture<LinkOtpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinkOtpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
