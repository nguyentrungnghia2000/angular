import { NgModule } from "@angular/core";
import { FrontendComponent } from "./frontend.component";
import { FrontendRoutingModule } from "./frontend-routing.module";
import { AppSharedModule } from "src/app/_share/appShare.module";

@NgModule({
    declarations:[
      FrontendComponent
    ],
    imports:[
      FrontendRoutingModule
    ],
    exports:[]
})

export class FrontendModule {}
