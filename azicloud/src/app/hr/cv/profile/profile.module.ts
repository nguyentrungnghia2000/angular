import { NgModule } from "@angular/core";
import { ProfileRoutingModule } from "./profile-routing.module";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//ng-zorro
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSelectModule } from 'ng-zorro-antd/select';

//Process
import {MatProgressBarModule} from '@angular/material/progress-bar';

//primeNG
import { DialogModule } from 'primeng/dialog';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {TabMenuModule} from 'primeng/tabmenu';
import {InputSwitchModule} from 'primeng/inputswitch';
import {CalendarModule} from 'primeng/calendar';
import { ButtonModule } from "primeng/button";
import {DropdownModule} from 'primeng/dropdown';
import {EditorModule} from 'primeng/editor';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputNumberModule} from 'primeng/inputnumber';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {ProgressBarModule} from 'primeng/progressbar';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {TimelineModule} from 'primeng/timeline';

//modules
import { ShareModule } from "./_share/share.module";

//components
import { ProfileComponent } from "./profile.component";
import { EmployeeEntryInfoComponent } from './pages/employee-entry-info/employee-entry-info.component';
import { EmployeeEntryContactComponent } from './pages/employee-entry-contact/employee-entry-contact.component';
import { EmployeeEntryResumeComponent } from './pages/employee-entry-resume/employee-entry-resume.component';
import { InfoComponent } from './pages/employee-entry-info/info/info.component';
import { HealthComponent } from './pages/employee-entry-info/health/health.component';
import { BankComponent } from './pages/employee-entry-info/bank/bank.component';
import { IntroComponent } from './pages/employee-entry-info/intro/intro.component';
import { CurrentPlaceComponent } from './pages/employee-entry-contact/current-place/current-place.component';
import { EmergenceContactComponent } from './pages/employee-entry-contact/emergence-contact/emergence-contact.component';
import { AddressComponent } from './pages/employee-entry-contact/address/address.component';
import { DependentComponent } from './pages/employee-entry-contact/dependent/dependent.component';
import { ExperiencesComponent } from './pages/employee-entry-resume/experiences/experiences.component';
import { SkillsComponent } from './pages/employee-entry-resume/skills/skills.component';
import { InfoEntryService } from "./_services/info-entry.service";
import { ResumeService } from "./pages/employee-entry-resume/_services/resume.service";

import { CommonModule } from "@angular/common";
import { CardIdComponent } from './pages/employee-entry-info/info/card-id/card-id.component';
import { PassportComponent } from './pages/employee-entry-info/info/passport/passport.component';
import { ConfirmationService } from "primeng/api";
import { AppSharedModule } from "src/app/_share/appShare.module";


@NgModule({
    declarations:[
       ProfileComponent,
       EmployeeEntryInfoComponent,
       EmployeeEntryContactComponent,
       EmployeeEntryResumeComponent,
       InfoComponent,
       HealthComponent,
       BankComponent,
       IntroComponent,
       CurrentPlaceComponent,
       EmergenceContactComponent,
       AddressComponent,
       DependentComponent,
       ExperiencesComponent,
       SkillsComponent,
       CardIdComponent,
       PassportComponent,

    ],
    imports:[
      AppSharedModule,
      CommonModule ,
      ProfileRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      ShareModule,
      BreadcrumbModule,
      TabMenuModule,
      InputSwitchModule,
      CalendarModule,
      ButtonModule,
      DropdownModule,
      NzTimelineModule,
      DialogModule,
      EditorModule,
      InputTextModule,
      InputTextareaModule,
      InputNumberModule,
      ScrollPanelModule,
      ProgressBarModule,
      ConfirmDialogModule,
      NzModalModule,
      MatProgressBarModule,
      NzDatePickerModule,
      NzSelectModule,
      TimelineModule

    ],
    providers:[
      InfoEntryService,
      ResumeService,
      ConfirmationService
    ],
    exports:[],
    entryComponents:[]
})

export class ProfileModule {}
