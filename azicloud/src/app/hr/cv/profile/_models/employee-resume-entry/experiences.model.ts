export class Experiences{
  id: number;
  type_id: number;
  type_name: string;
  employee_id: number;
  name: string;
  start_date: string;
  end_date: string;
  description: string;
}
