import { NgModule } from "@angular/core";
import { EmployeeRoutingModule } from "./employee-routing.module";
import { EmployeeComponent } from "./employee.component";
import { MainComponent } from './pages/main/main.component';
import { MainListComponent } from './pages/main-list/main-list.component';
import { MainService } from "./_services/main.service";
import { CommonModule } from "@angular/common";
@NgModule({
    declarations:[
      EmployeeComponent,
      MainComponent,
      MainListComponent
    ],
    imports:[
      EmployeeRoutingModule,
      CommonModule
    ],
    providers:[
      MainService
    ],
    exports:[]
})

export class EmployeeModule {}
