
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../_services/login.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService, UserState } from '../../_services/auth.service';
import { GoogleLoginProvider, SocialAuthService, FacebookLoginProvider, SocialUser } from 'angularx-social-login';

import  firebase from 'firebase';
import 'firebase/auth';
import 'firebase/database';
import { HttpClient } from '@angular/common/http';
import { Company } from '../../_models/company.module';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

// import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form:FormGroup;
  email: string = 'nguyentrungnghia.2000@gmail.com';
  password: string = '123456';
  _email: string = 'azicloudco@gmail.com';
  _password: string = 'ee96a2fa598d3a8a105c7ca86787f507';
  identifier: string ='';
  name: string ='';
  version: string ='';
  userAgent: string = '';
  showPass = false;
  showEye = false;

  activatedDisplay = false;
  linkedDisplay = false;
  registedDisplay = false;
  regisWorkPlace = false;

  companyList: Company[] = [];
  company: Company;

  lick=null
  firebaseAuth = firebase.auth();
  // provider = new OAuthProvider();

  browser_uid: string;

  constructor(private auth: AuthService,
              private loginsService: LoginService,
              private socialAuthService: SocialAuthService,
              private router: Router,
              private fb: FormBuilder,
              private http: HttpClient,
              private toastr: ToastrService,
              private cookieService: CookieService) {
                let register_email = localStorage.getItem('register_email');
                this.browser_uid = this.cookieService.get('browser_uid');
                if(this.browser_uid === null || this.browser_uid === undefined || this.browser_uid === ''){
                  this.browser_uid = Guid.newGuid();
                  this.cookieService.set('browser_uid', this.browser_uid);
                }
                if(register_email !== null && register_email !== undefined){
                  this.email = register_email;
                }
              }
  ngOnInit(): void {
    // this.getIpAddress();
    this.getUserAgent();
    // this.browserCreate();
  }

  onSubmit(): void{
    this.lick = true
    this.onClick();
  }

  browserCreate(){
    this.loginsService.createBrowser(
      this.identifier,
      this.name,
      this.version,
      this.userAgent
      ).subscribe((res: any) =>{
        if(res !== null){
          console.log("browser create successfully");
        } else {
          console.log("browser create unsuccessfully");
        }

    })
  }

  getUserAgent(){
    //user-agent
    this.userAgent = window.navigator.userAgent;
    //version
    let posChrome = this.userAgent.indexOf("Chrome");
    let temp = this.userAgent.substring(posChrome+7);
    let strVersion: string[] = temp.split(" ");
    this.version = strVersion[0];
    //name
    let startPos = this.userAgent.indexOf("(");
    let endPos = this.userAgent.indexOf(";");
    this.name = this.userAgent.substring(startPos+1, endPos);
  }

  getIpAddress(){
    //identifier
    this.http.get("http://api.ipify.org/?format=json").subscribe((res: any)=>{
      this.identifier = res.ip;
    })
  }

  onClick(): void {
    if(this.email !== '' && this.password !== ''){
      this.loginsService.login(this.email, this.password, this.browser_uid).subscribe((res: any) =>{
        if(res.status === 1){
          localStorage.removeItem('register_email');
          //call api login create
          this.auth.loginCreate(0, res.refresh_token, this.browser_uid, 'string', 0, 0, 'string').subscribe((res: any)=>{
            if(res !== null){
              console.log(res);
            }
          })
          //check company before login
          if(res.company.length > 0){
            if(res.company.length === 1){
              localStorage.setItem("usercompany", JSON.stringify(res.company[0]));
              this.router.navigate(['/hr']);
            } else{
              for(let i = 0; i < res.company.length; i++){
                this.company = new Company();
                this.company = res.company[i];
                this.companyList.push(this.company);
              }
              localStorage.setItem("listCompany", JSON.stringify(this.companyList));
              this.router.navigate(['/core/frontend/auth/choose']);
            }
          } else {
            this.regisWorkPlace = true;
          }

        } else if(res.status === -3){
          localStorage.setItem('user_name', this.email);
          this.activatedDisplay = true;
        } else if(res.status === -1){
          this.registedDisplay = true;
        } else if(res.status === -4){
          this.linkedDisplay = true;
        } else if(res.status === -2){
          this.toastr.error('Mật khẩu chưa đúng. Xin thử lại', 'LỖI');
        }
      });
    } else{
      this.toastr.error('Vui lòng nhập đầy đủ thông tin để đăng nhập', 'LỖI');
    }

  }

  loginWithFacebook(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((userData: any) => {
      this.loginsService.loginSocial( userData.id, userData.name,
                                      'facebook', this.browser_uid).subscribe( (res: any) => {
          if(res.status === 1){

            //check company before login
            if(res.company.length > 0){
              if(res.company.length === 1){
                localStorage.setItem("usercompany", JSON.stringify(res.company[0]));
                this.router.navigate(['/hr']);
              } else{
                for(let i = 0; i < res.company.length; i++){
                  this.company = new Company();
                  this.company = res.company[i];
                  this.companyList.push(this.company);
                }
                localStorage.setItem("listCompany", JSON.stringify(this.companyList));
                this.router.navigate(['/core/frontend/auth/choose']);
              }
            } else {
              this.regisWorkPlace = true;
            }
            localStorage.setItem('user', JSON.stringify(res.employee_id));
            localStorage.setItem('user_id', JSON.stringify(res.id));
            this.toastr.success('Đăng nhập thành công', 'THÔNG BÁO')
            this.router.navigate(['/hr']);
          } else if(res.status === -2){
            localStorage.setItem('facebook_email', userData.email);
            this.registedDisplay = true;
          }  else if(res.status === -5){
            localStorage.setItem('user_name', res.username);
            this.activatedDisplay = true;
          } else if(res.status === -6){
            localStorage.setItem('user_id', res.id);
            localStorage.setItem('name', res.name);
            localStorage.setItem('social_account', JSON.stringify(userData));
            this.linkedDisplay = true;
          } else if(res.status === -7){
            localStorage.setItem('user_id', res.id);
            localStorage.setItem('name', res.name);
            localStorage.setItem('social_account', JSON.stringify(userData));
            this.linkedDisplay = true;
          }
        },
        error => {error.message;}
      );
    })
  }

  loginWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData: any) => {
      this.loginsService.loginSocial( userData.id, userData.email,
                                      'google', this.browser_uid).subscribe( (res: any) => {
          if(res.status === 1){

            //check company before login
            if(res.company.length > 0){
              if(res.company.length === 1){
                localStorage.setItem("usercompany", JSON.stringify(res.company[0]));
                this.router.navigate(['/hr']);
              } else{
                for(let i = 0; i < res.company.length; i++){
                  this.company = new Company();
                  this.company = res.company[i];
                  this.companyList.push(this.company);
                }
                localStorage.setItem("listCompany", JSON.stringify(this.companyList));
                this.router.navigate(['/core/frontend/auth/choose']);
              }
            } else {
              this.regisWorkPlace = true;
            }
            localStorage.setItem('user', JSON.stringify(res.employee_id));
            localStorage.setItem('user_id', JSON.stringify(res.id));
            this.toastr.success('Đăng nhập thành công', 'THÔNG BÁO')
            this.router.navigate(['/hr']);
          } else if(res.status === -2){
            localStorage.setItem('google_email', userData.email);
            this.registedDisplay = true;
          } else if(res.status === -5){
            this.activatedDisplay = true;
          } else if(res.status === -6){
            localStorage.setItem('user_id', res.id);
            localStorage.setItem('name', res.name);
            localStorage.setItem('social_account', JSON.stringify(userData));
            this.linkedDisplay = true;
          } else if(res.status === -7){
            localStorage.setItem('user_id', res.id);
            localStorage.setItem('name', res.name);
            localStorage.setItem('social_account', JSON.stringify(userData));
            this.linkedDisplay = true;
          }
        },
        error => {error.message;}
      );
    })
  }

  async signInWithApple() {

    let provider = new firebase.auth.OAuthProvider('apple.com');
    await this.firebaseAuth.signInWithPopup(provider).then((userData: any) => {
      console.log(userData);
      this.loginsService.loginSocial( userData.user.uid, userData.user.email,
                                      'apple', this.browser_uid).subscribe((res: any) => {
          console.log(res);
          if(res.status === 1){

            //check company before login
            if(res.company.length > 0){
              if(res.company.length === 1){
                localStorage.setItem("usercompany", JSON.stringify(res.company[0]));
                this.router.navigate(['/hr']);
              } else{
                for(let i = 0; i < res.company.length; i++){
                  this.company = new Company();
                  this.company = res.company[i];
                  this.companyList.push(this.company);
                }
                localStorage.setItem("listCompany", JSON.stringify(this.companyList));
                this.router.navigate(['/core/frontend/auth/choose']);
              }
            } else {
              this.regisWorkPlace = true;
            }
            localStorage.setItem('user', JSON.stringify(res.employee_id));
            localStorage.setItem('user_id', JSON.stringify(res.id));
            this.toastr.success('Đăng nhập thành công', 'THÔNG BÁO')
            this.router.navigate(['/hr']);
          } else if(res.status === -2){
            localStorage.setItem('apple_email', userData.email);
            this.registedDisplay = true;
          } else if(res.status === -5){
            this.activatedDisplay = true;
          } else if(res.status === -6){
            localStorage.setItem('user_id', res.id);
            localStorage.setItem('name', res.name);
            localStorage.setItem('social_account', JSON.stringify(userData));
            this.linkedDisplay = true;
          } else if(res.status === -7){
            localStorage.setItem('user_id', res.id);
            localStorage.setItem('name', res.name);
            localStorage.setItem('social_account', JSON.stringify(userData));
            this.linkedDisplay = true;
          }
        })
    })
  };

  closeDialog(){
    this.activatedDisplay = false;
    this.linkedDisplay = false;
    this.registedDisplay = false;
    this.regisWorkPlace = false;
  }

  moveToActivate(){
    this.loginsService.renewOTP(this.email, 'string').subscribe((res: any) => {
      if(res.result === 1){
        this.closeDialog();
        this.router.navigate(['/core/frontend/auth/otp']);
      }
    });
  }

  moveToRegister(){
    this.closeDialog();
    this.router.navigate(['/core/frontend/auth/signup']);
  }

  linkAccount(){
    this.closeDialog();
    this.router.navigate(['/core/frontend/auth/link']);
  }

  moveToRegisWorkPlace(){
    this.closeDialog();
    this.router.navigate(['/core/frontend/auth/regisworkplace']);
  }

  showPassword(){

    this.showPass = !this.showPass;
    this.showEye = !this.showEye;
  }

}


//class create uid
class Guid {
  static newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}
