import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergenceContactComponent } from './emergence-contact.component';

describe('EmergenceContactComponent', () => {
  let component: EmergenceContactComponent;
  let fixture: ComponentFixture<EmergenceContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmergenceContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergenceContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
