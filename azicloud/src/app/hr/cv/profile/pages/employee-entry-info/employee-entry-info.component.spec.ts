import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeEntryInfoComponent } from './employee-entry-info.component';

describe('EmployeeEntryInfoComponent', () => {
  let component: EmployeeEntryInfoComponent;
  let fixture: ComponentFixture<EmployeeEntryInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeEntryInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeEntryInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
