// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endPoint: "http://api.azicloud.vn/api/v1",
  firebase: {
    apiKey: "AIzaSyCxVcA0LTDMPHQm7rQ0NRcwEWN9qgj6y-U",
    authDomain: "azicloud.firebaseapp.com",
    projectId: "azicloud",
    storageBucket: "azicloud.appspot.com",
    messagingSenderId: "715576938476",
    appId: "1:715576938476:web:9b3a454892d8390dfc7ac9",
    measurementId: "G-84C335M3BF"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
