import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { concatMap, tap } from 'rxjs/operators';
import { Company } from 'src/app/core/frontend/auth/_models/company.module';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { Passport } from '../../../../_models/employee-info-entry/employee-passport';
import { ImageUpLoad } from '../../../../_models/image.model';
import { InfoEntryService } from '../../../../_services/info-entry.service';

@Component({
  selector: 'app-passport',
  templateUrl: './passport.component.html',
  styleUrls: ['./passport.component.scss']
})
export class PassportComponent implements OnInit {

  display: boolean = false;
  loadData = false;
  frontOk = false;
  backOk = false;
  userId!: any;
  uid: any;
  employeePassport: Passport;
  employeePassportUpdate: Passport;
  updateDateChecked = false;
  updateDateExpire = false;

  frontImage: File = null;
  backImage: File = null;
  frontUrl: any;
  backUrl: any;
  frontImageUpload: ImageUpLoad;
  backImageUpload: ImageUpLoad;
  frontImageLoad: string;
  backImageLoad: string;

  passportStartDate: Date;
  passportEndDate: Date;
  userCompany: Company;

  loading$ = this.loaderService.isLoading$;

  constructor(private infoEntryService: InfoEntryService,
              private toastr: ToastrService,
              public loaderService: LoaderService,
              private renderer: Renderer2,
              @Inject(DOCUMENT) private doc: Document) {
      this.userId = localStorage.getItem('user');
      this.uid = parseInt(this.userId);
      this.userCompany = new Company();
      this.userCompany = JSON.parse(localStorage.getItem('usercompany'));
      this.employeePassport = new Passport();
      this.frontImageUpload = new ImageUpLoad();
      this.backImageUpload = new ImageUpLoad();
   }

  ngOnInit(): void {
    this.loadEmployeePassport();
  }

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  //open dialog
  showDialog() {
    this.frontUrl = `http://api.azicloud.vn/${this.employeePassport.passport_front_image_filepath}${this.employeePassport.passport_front_image_filename}`;
    this.backUrl = `http://api.azicloud.vn/${this.employeePassport.passport_backside_image_filepath}${this.employeePassport.passport_backside_image_filename}`;
    this.employeePassportUpdate = new Passport();
    this.loadEmployeePassportUpdate();
    this.display = true;
    this.updateBodyPadding();
  }

  closeDialog(){
    this.display = false;
    this.frontUrl = null;
    this.backUrl = null;
    this.updateBodyPadding(true);
  }

  loadEmployeePassport(){
    this.loadData = true;
    this.infoEntryService.getEmployeePassport(this.uid).subscribe((res: any) => {
      if(res !== null){
        this.loadImagePassport(res);
        this.loadData = false;
      }
      else{
        this.toastr.error('Không thể tải về thông tin hộ chiếu', 'LỖI');
      }
    });
  }

  loadEmployeePassportUpdate(){
    this.infoEntryService.getEmployeePassport(this.uid).subscribe((res: any) => {
      if(res !== null){
        this.employeePassportUpdate = res;
        this.passportStartDate = new Date(this.employeePassportUpdate.passport_start_date);
        this.passportEndDate = new Date(this.employeePassportUpdate.passport_end_date);
      }
      else{
        this.toastr.error('Không thể tải về thông tin hộ chiếu', 'LỖI');
      }
    });
  }

  //load passport image
  loadImagePassport(res: any){
    this.employeePassport = res;
    this.employeePassport.passport_start_date = this.getDateOnly(this.employeePassport.passport_start_date);
    this.employeePassport.passport_end_date = this.getDateOnly(this.employeePassport.passport_end_date);
    this.frontImageLoad = `http://api.azicloud.vn/${this.employeePassport.passport_front_image_filepath}${this.employeePassport.passport_front_image_filename}`;
    this.backImageLoad = `http://api.azicloud.vn/${this.employeePassport.passport_backside_image_filepath}${this.employeePassport.passport_backside_image_filename}`;
  }

  onUpdate(){

    //automatic convert date to JSON-DATE if user dont update date
    if(!this.updateDateChecked || !this.updateDateExpire){
      if(!this.updateDateExpire){
        let d = new Date(Date.parse(this.employeePassportUpdate.passport_start_date));
        this.employeePassportUpdate.passport_start_date = d.toISOString();
      } else{
        let d = new Date(Date.parse(this.employeePassportUpdate.passport_start_date));
        this.employeePassportUpdate.passport_start_date = d.toISOString();
      }
    }

    //conditions for image (if there are image imported or not)
    if(this.frontOk === false && this.backOk === false){
      this.updateEmployeePassport();
    } else{
      if(this.frontOk === true && this.backOk === true){
        this.updateWithFrontAndBackImageAdded();
      }else if(this.frontOk === true && this.backOk === false){
        this.updateWithFrontImageAdded();
      }else if(this.frontOk === false && this.backOk === true){
        this.updateWithBackImageAdded();
      }
    }
  }

  updateEmployeePassport(){
    this.infoEntryService.updateEmployeePassport(this.uid, this.employeePassportUpdate.passport, this.employeePassportUpdate.passport_start_date,
                                                  this.employeePassportUpdate.passport_end_date, this.employeePassportUpdate.passport_issued,
                                                  this.employeePassportUpdate.passport_front_image_id,
                                                  this.employeePassportUpdate.passport_backside_image_id).subscribe((res: any) => {
      if(res.result === 2){
        this.frontUrl = null;
        this.backUrl = null;
        this.frontOk = false;
        this.backOk = false;
        this.updateDateChecked = false;
        this.updateDateExpire = false;
        this.closeDialog();
        this.toastr.success('Cập nhật hộ chiếu thành công', 'THÔNG BÁO');
      }
      else{
        this.toastr.error('Cập nhật hộ chiếu thất bại', 'LỖI');
      }
    });
  }

  //convert start date to JSON string
  onSelectedStartDateUpdate(event){
    let d = new Date(Date.parse(event));
    this.employeePassportUpdate.passport_start_date = d.toISOString();
    this.updateDateChecked = true;
  }

  //convert end date to JSON string
  onSelectedEndDateUpdate(event){
    let d = new Date(Date.parse(event));
    this.employeePassportUpdate.passport_end_date = d.toISOString();
    this.updateDateExpire = true;
  }

  //split date only to show on label
  getDateOnly(str: string){
    let arrTemp: string[] = str.split('T');
    return arrTemp[0];
  }

  //get file from uploader input
  onSelectedFrontImage(event){
    this.frontImage = event.target.files[0];

    if(!event.target.files[0] || event.target.files[0].length == 0) {
			return;
		}
    let mimeType = event.target.files[0].type;
		if (mimeType.match(/image\/*/) == null) {
			this.toastr.warning('Hệ thống chỉ hỗ trợ mục hình ảnh', 'CẢNH BÁO');
			return;
		}

		let reader = new FileReader();
		reader.readAsDataURL(this.frontImage);
		reader.onload = () => {
			this.frontUrl = reader.result;
      this.frontOk = true;
		}
  }

  //get file from uploader input
  onSelectedBackImage(event){
    this.backImage = event.target.files[0];

    if(!event.target.files[0] || event.target.files[0].length == 0) {
			return;
		}
    let mimeType = event.target.files[0].type;
		if (mimeType.match(/image\/*/) == null) {
			this.toastr.warning('Hệ thống chỉ hỗ trợ mục hình ảnh', 'CẢNH BÁO');
			return;
		}

		let reader = new FileReader();
		reader.readAsDataURL(this.backImage);
		reader.onload = () => {
			this.backUrl = reader.result;
      this.backOk = true;
		}
  }

  updateWithBackImageAdded(){
    this.infoEntryService.uploadImage(this.uid, this.userCompany.url, this.backImage).pipe(
      tap((res: any) => {
        if(res.result === 1){
          this.backImageUpload = res;
          this.employeePassportUpdate.passport_backside_image_id = this.backImageUpload.id;
          this.backOk = true;
          this.backImage = null;
          this.toastr.success('Đã nhận ảnh mặt sau', 'THÔNG BÁO');
        } else{
          this.toastr.error('Chưa nhận được ảnh mặt sau', 'LỖI');
        }
      }),
      concatMap((result: any) => this.infoEntryService.updateEmployeePassport(this.uid, this.employeePassportUpdate.passport,
                                                                              this.employeePassportUpdate.passport_start_date,
                                                                              this.employeePassportUpdate.passport_end_date,
                                                                              this.employeePassportUpdate.passport_issued,
                                                                              this.employeePassportUpdate.passport_front_image_id,
                                                                              this.employeePassportUpdate.passport_backside_image_id)),
      tap((res: any) => {
        if(res.result === 2){
          this.toastr.success('Cập nhật chứng minh nhân dân thành công', 'THÔNG BÁO');
          this.closeDialog();
          this.frontUrl = null;
          this.backUrl = null;
          this.frontOk = false;
          this.backOk = false;
          this.updateDateChecked = false;
          this.loadEmployeePassport();
        } else{
          this.toastr.error('Cập nhật chứng minh nhân dân thất bại', 'LỖI');
        }
      })
    ).subscribe((data: any) => {
      console.log('data');
    })
  }

  updateWithFrontImageAdded(){
    this.infoEntryService.uploadImage(this.uid, this.userCompany.url, this.frontImage).pipe(
      tap((res: any) => {
        if(res.result === 1){
          this.frontImageUpload = res;
          this.employeePassportUpdate.passport_front_image_id = this.frontImageUpload.id;
          this.frontOk = true;
          this.frontImage = null;
          this.toastr.success('Đã nhận ảnh mặt trước', 'THÔNG BÁO');
        } else{
          this.toastr.error('Chưa nhận được ảnh mặt trước', 'LỖI');
        }
      }),
      concatMap((result: any) => this.infoEntryService.updateEmployeePassport(this.uid, this.employeePassportUpdate.passport,
                                                                              this.employeePassportUpdate.passport_start_date,
                                                                              this.employeePassportUpdate.passport_end_date,
                                                                              this.employeePassportUpdate.passport_issued,
                                                                              this.employeePassportUpdate.passport_front_image_id,
                                                                              this.employeePassportUpdate.passport_backside_image_id)),
      tap((res: any) => {
        if(res.result === 2){
          this.toastr.success('Cập nhật chứng minh nhân dân thành công', 'THÔNG BÁO');
          this.closeDialog();
          this.frontUrl = null;
          this.backUrl = null;
          this.frontOk = false;
          this.backOk = false;
          this.updateDateChecked = false;
          this.loadEmployeePassport();
        } else{
          this.toastr.error('Cập nhật chứng minh nhân dân thất bại', 'LỖI');
        }
      })
    ).subscribe((data: any) => {
      console.log('data');
    })
  }

  updateWithFrontAndBackImageAdded(){
    this.infoEntryService.uploadImage(this.uid, this.userCompany.url, this.frontImage).pipe(
      tap((res: any) => {
        if(res.result === 1){
          this.frontImageUpload = res;
          this.employeePassportUpdate.passport_front_image_id = this.frontImageUpload.id;
          this.frontOk = true;
          this.frontImage = null;
          this.toastr.success('Đã nhận ảnh mặt trước', 'THÔNG BÁO');
        } else{
          this.toastr.error('Chưa nhận được ảnh mặt trước', 'LỖI');
        }
      }),
      concatMap((result: any) => this.infoEntryService.uploadImage(this.uid, this.userCompany.url, this.backImage)),
      tap((res: any) => {
        if(res.result === 1){
          this.backImageUpload = res;
          this.employeePassportUpdate.passport_backside_image_id = this.backImageUpload.id;
          this.backOk = true;
          this.backImage = null;
          this.toastr.success('Đã nhận ảnh mặt sau', 'THÔNG BÁO');
        } else{
          this.toastr.error('Chưa nhận được ảnh mặt sau', 'LỖI');
        }
      }),
      concatMap((result: any) => this.infoEntryService.updateEmployeePassport(this.uid, this.employeePassportUpdate.passport,
                                                                              this.employeePassportUpdate.passport_start_date,
                                                                              this.employeePassportUpdate.passport_end_date,
                                                                              this.employeePassportUpdate.passport_issued,
                                                                              this.employeePassportUpdate.passport_front_image_id,
                                                                              this.employeePassportUpdate.passport_backside_image_id)),
      tap((res: any) => {
        if(res.result === 2){
          this.toastr.success('Cập nhật chứng minh nhân dân thành công', 'THÔNG BÁO');
          this.closeDialog();
          this.frontUrl = null;
          this.backUrl = null;
          this.frontOk = false;
          this.backOk = false;
          this.updateDateChecked = false;
          this.loadEmployeePassport();
        } else{
          this.toastr.error('Cập nhật chứng minh nhân dân thất bại', 'LỖI');
        }
      })
    ).subscribe((data: any) => {
      console.log('data');
    })
  }

}
