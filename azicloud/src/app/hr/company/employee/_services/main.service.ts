import { Injectable } from '@angular/core';
import { API } from 'src/app/_share/api/api.hr.config';
import { HttpService } from 'src/app/_share/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  token = '';

  constructor(private http: HttpService) {
    this.token = localStorage.getItem('access_token');
  }

  getListEmployee(search_text){
    return this.http.sendToServer('GET', API.COMPANY.EMPLOYEE.LIST(search_text), null, {'Authorization': `Bearer ${this.token}`});
  }

  getListEmployeeWithoutText(){
    return this.http.sendToServer('GET', API.COMPANY.EMPLOYEE.LIST_WITHOUT_TEXT, null, {'Authorization': `Bearer ${this.token}`});
  }
}
