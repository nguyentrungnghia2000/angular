import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  items: MenuItem[];
    
  activeItem: MenuItem;

  constructor() { }

  ngOnInit(): void {
    this.items = [
      {label: 'HỒ SƠ'},
      {label: 'CÔNG VIỆC'},
      {label: 'CHẤM CÔNG'},
      {label: 'YÊU CẦU'},
      {label: 'THI ĐUA KHEN THƯỞNG'},
      {label: 'THỜI GIAN BIỂU'},
      {label: 'TÀI LIỆU'},
      {label: 'TÙY CHỈNH'}
  ];
  
  this.activeItem = this.items[0];
  }

}
