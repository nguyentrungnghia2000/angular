export const API ={
    USER: {
        LOGIN:"/user/login",
        LOGOUT:"/user/logout",
        LOGIN_CREATE:"/user/login/create",

        LOGIN_SOCIAL:"/user/login-social",
        SOCIAL_CONFIRM:"/user/link-social/confirm",
        SOCIAL_CREATE: "/user/link-social/create",

        FORGOT_PASSWORD:"/user/forgot-password",
        TOKEN_TIMEOUT:"/user/token-timeout",
        CHANGE_PASSWORD:"/user/change-password",
        REFRESH_TOKEN:"/user/refresh-token",
        REGISTER:"/user/register",
        VERIFY:"/user/verify",
        BROWSER: "/user/browser/create",

        OTP_RENEW: "/user/token-otp-renew",
        OTP_AVAILABLE: "/user/token-otp-available",
        COMPANY_QUICK_CREATE:"/res/company/quick-create"
    }
}
