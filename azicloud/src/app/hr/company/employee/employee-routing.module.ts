import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EmployeeComponent } from "./employee.component";
import { MainListComponent } from "./pages/main-list/main-list.component";
import { MainComponent } from "./pages/main/main.component";

const routes: Routes =[
    { path:'', component:EmployeeComponent,
      children:[
        {path: "main", component: MainListComponent},
        // {path: "main/:text", component: MainListComponent},
        { path:'', pathMatch:'full', redirectTo:'main' }
      ]
    }
]
@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class EmployeeRoutingModule {}
