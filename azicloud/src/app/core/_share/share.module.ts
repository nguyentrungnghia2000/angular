import { NgModule } from "@angular/core";

//ngzorro
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
@NgModule({
    declarations:[],
    imports:[
        NzDropDownModule
    ],
    exports:
    [
        NzDropDownModule
    ]

})
export class ShareModule{}
