import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { Employee } from '../../../_models/employee-info-entry/employee';
import { InfoEntryService } from '../../../_services/info-entry.service';
import { concatMap, map, switchMap, tap } from 'rxjs/operators'
import { Country } from '../../../_models/dropdown/country';
import { pipe } from 'rxjs';
import { Configs } from '../../../_models/dropdown/config';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { AuthService } from 'src/app/core/frontend/auth/_services/auth.service';
// import { ToolbarService, LinkService, ImageService, HtmlEditorService } from '@syncfusion/ej2-angular-richtexteditor';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  providers: [ ]
})
export class InfoComponent implements OnInit {

  employee!: Employee;
  employeeUpdate!: Employee;
  display: boolean = false;
  loadData = false;
  userId!: any;
  uid: any;
  loading: any;

  date:Date;
  ListCountry: Country[] =[];
  contry: Country;
  ListGenders: Configs[] = [];
  gender: Configs;
  ListMaritals: Configs[] = [];
  marital: Configs;
  ListNations: Configs[] = [];
  nation: Configs;

  loading$ = this.loaderService.isLoading$;

  constructor(private infoEntryService: InfoEntryService,
              public loaderService: LoaderService,
              private toastr: ToastrService,
              private renderer: Renderer2,
              private authService: AuthService,
              @Inject(DOCUMENT) private doc: Document) {
    this.employee = new Employee();
    this.userId = localStorage.getItem('user');
    this.uid = parseInt(this.userId);
    this.getListCountries();
    this.getListGenders();
    this.getListMaritals();
    this.getListNations();

   }

  ngOnInit(): void {
    this.loadPrivateInfo();
    this.loading = this.loaderService.getLoading();
  }

  //get employee infomation
  loadPrivateInfo(){
    this.loadData = true;
    this.infoEntryService.getEmployeeInfo(this.uid).subscribe((res: any) => {
      if(res !== null){
        this.employee = res;
        this.loadData = false;
      } else{
        this.toastr.error('Không thể tải về thông tin người dùng', 'LỖI');
      }
    });
  }

  loadPrivateInfoUpdate(){
    this.infoEntryService.getEmployeeInfo(this.uid).subscribe((res: any) => {
      if(res !== null){
        this.employeeUpdate = res;
        if(this.employeeUpdate.birth_year === 0 || this.employeeUpdate.birth_day === 0 || this.employeeUpdate.birth_month === 0){
          this.date = new Date();
        } else {
          this.date = new Date(`${this.employeeUpdate.birth_year}-${this.employeeUpdate.birth_month}-${this.employeeUpdate.birth_day}`);
        }
      } else{
        this.toastr.error('Không thể tải về thông tin người dùng', 'LỖI');
      }
    });
  }

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  //update employee infomation
  updatePrivateInfo(){

    this.infoEntryService.updateEmployeeInfo(this.uid, this.employeeUpdate.name, this.employeeUpdate.phone, this.employeeUpdate.email, this.employeeUpdate.tax_code,
                                                this.employeeUpdate.birth_day, this.employeeUpdate.birth_month, this.employeeUpdate.birth_year, this.employeeUpdate.nationality_id,
                                                this.employeeUpdate.religion_name, this.employeeUpdate.nation_key, this.employeeUpdate.gender_key, this.employeeUpdate.marital_status_key,
                                                this.employeeUpdate.active, true).subscribe((res: any) =>{
      if(res.result === 1){
        this.loadPrivateInfo();
        this.toastr.success('Cập nhật thông tin nhân viên thành công', 'THÔNG BÁO');
        this.closeDialog();
      } else {
        this.toastr.error('Cập nhật thông tin nhân viên thất bại', 'LỖI');
      }

    });
  }

  //get list countries for combobox
  getListCountries(){
    this.infoEntryService.getListCountries().pipe(map((result: any) =>
    result.map((data: any) => {
      return this.ListCountry.push(this.contry = new Country(data.name, data.id));
    }))).subscribe((res: any) =>{
      if(res !== null){
      }
      else{
        this.toastr.error('Tải danh sách các quốc gia thất bại', 'LỖI');
      }
    })
  }

  //get list genders for combobox
  getListGenders(){
    this.infoEntryService.getListGenders().pipe(map((result: any)=>
    result.map((data: any) =>{
      return this.ListGenders.push(this.gender = new Configs(data.value, data.key));
    }))).subscribe((res: any) =>{
      if(res !== null){
      }
      else{
        this.toastr.error('Tải danh sách giới tính thất bại', 'LỖI');
      }
    })
  }

  //get list nations for combobox
  getListNations(){
    this.infoEntryService.getListNations().pipe(map((result: any)=>
    result.map((data: any) =>{
      return this.ListNations.push(this.nation = new Configs(data.value, data.key));
    }))).subscribe((res: any) =>{
      if(res !== null){
      }
      else{
        this.toastr.error('Tải danh sách dân tộc thất bại', 'LỖI');
      }
    })
  }

  //get list maritals for combobox
  getListMaritals(){
    this.infoEntryService.getListMaritals().pipe(map((result: any)=>
    result.map((data: any) =>{
      return this.ListMaritals.push(this.marital = new Configs(data.value, data.key));
    }))).subscribe((res: any) =>{
      if(res !== null){
      }
      else{
        this.toastr.error('Tải danh sách tình trạng hôn nhân thất bại', 'LỖI');
      }
    })
  }

  //show-display dialog
  showDialog() {
    this.employeeUpdate = new Employee();
    this.loadPrivateInfoUpdate();
    this.display = true;
    this.updateBodyPadding();
  }

  closeDialog(){
    this.display = false;
    this.updateBodyPadding(true);
  }

  //slipt selection to three values: day, month, year
  onSelected(event){
    let d = new Date(Date.parse(event));
    let strDate = d.toString();
    let arrDate: string[] = strDate.split(" ");
    this.employeeUpdate.birth_day = parseInt(arrDate[2]);
    this.employeeUpdate.birth_year = parseInt(arrDate[3]);
    switch(arrDate[1]){
      case "Jan":
        this.employeeUpdate.birth_month = 1
        break;
      case "Feb":
        this.employeeUpdate.birth_month = 2
        break;
      case "Mar":
        this.employeeUpdate.birth_month = 3
        break;
      case "Apr":
        this.employeeUpdate.birth_month = 4
        break;
      case "May":
        this.employeeUpdate.birth_month = 5
        break;
      case "Jun":
        this.employeeUpdate.birth_month = 6
        break;
      case "Jul":
        this.employeeUpdate.birth_month = 7
        break;
      case "Aug":
        this.employeeUpdate.birth_month = 8
        break;
      case "Sep":
        this.employeeUpdate.birth_month = 9
        break;
      case "Oct":
        this.employeeUpdate.birth_month = 10
        break;
      case "Nov":
        this.employeeUpdate.birth_month = 11
        break;
      case "Dec":
        this.employeeUpdate.birth_month = 12
        break;
    }
  }

  //get marital.key from marital value
  onChangeMarital(event){
    let changedValue = event;
    for(let i = 0; i < this.ListMaritals.length; i++){
      if(this.ListMaritals[i].value === changedValue){
        this.employeeUpdate.marital_status_key = this.ListMaritals[i].key;
      }
    }
  }

  //get gender.key from gender.value
  onChangeGender(event){
    let changedValue = event;
    for(let i = 0; i < this.ListGenders.length; i++){
      if(this.ListGenders[i].value === changedValue){
        this.employeeUpdate.gender_key = this.ListGenders[i].key;

      }
    }
  }

  //get nation.key from nation.value
  onChangeNation(event){
    let changedValue = event
    for(let i = 0; i < this.ListNations.length; i++){
      if(this.ListNations[i].value === changedValue){
        this.employeeUpdate.nation_key = this.ListNations[i].key;
      }
    }
  }

  //get country.id from country.name
  onChangeCountry(event){
    let changedValue = event;
    for(let i = 0; i < this.ListCountry.length; i++){
      if(this.ListCountry[i].name === changedValue){
        this.employeeUpdate.nationality_id = this.ListCountry[i].id;
      }
    }
  }
}

