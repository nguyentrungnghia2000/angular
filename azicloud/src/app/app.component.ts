import { Component } from '@angular/core';
import { LoaderService } from './_share/services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'azicloud';

  constructor(public loaderService: LoaderService){}
}
