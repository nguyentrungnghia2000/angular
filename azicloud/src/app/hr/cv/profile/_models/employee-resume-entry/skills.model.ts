export class Skills{
  employee_id: number;
  level_process: number;
  skill_type_name: string;
  skill_name: string;
  skill_level_name: string;
  id: number;
  skill_id: number;
  skill_type_id: number;
  skill_level_id: number;
}
