import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NewPasswordService } from '../../_services/new-password.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  username: string = '';

  constructor(private newPasswordService: NewPasswordService,
              private router: Router,
              private fb: FormBuilder,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }
  infoUser = this.fb.group({
    email: ['', [Validators.required,Validators.email]]

    })
   get f(){
      return this.infoUser.controls
    }

  onSubmit(): void{
    this.onClick();
  }

  onClick(): void{
    if(this.username !== ''){
      this.newPasswordService.forgotPassword(this.username).subscribe((res: any)=>{
        if(res.result === 1){
          this.toastr.success('Kiểm tra mã xác thực trong email của bạn', 'THÔNG BÁO');
          localStorage.setItem('username', this.username);
          this.router.navigate(['/core/frontend/auth/tokentimeout']);
        }
      });
    } else {
      this.toastr.error('Vui lòng nhập tài khoản cần đổi mật khẩu', 'LỖI');
    }

  }

}
