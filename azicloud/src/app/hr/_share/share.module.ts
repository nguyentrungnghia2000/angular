import { NgModule } from "@angular/core";
import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from './header/header.component';

//ngzorro
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
@NgModule({
    declarations:[FooterComponent, HeaderComponent],
    imports:[
        NzDropDownModule
    ],
    exports:
    [
        HeaderComponent,
        FooterComponent,
        NzDropDownModule
    ]

})
export class ShareModule{}
