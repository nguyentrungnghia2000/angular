export class User{
    id: number;
    username: string;
    password: string;
    accessToken: string;
    expires: string;
    refreshToken: string;
    status: number;
    employee_id: number;
    company_code: string;
    company_id: number;
    browser_uid: string;
    constructor(obj?: any) {
      this.id = obj?._id || '';
      this.username = obj?.username || '';
      this.password = obj?.password || '';
      this.accessToken = obj?.accessToken || '';
      this.expires = obj?.expires || '';
      this.refreshToken = obj?.refreshToken || '';
      this.status = obj?.status || '';
      this.employee_id = obj?.employee_id || '';
      this.company_code = obj?.company_code || '';
      this.company_id = obj?.company_id || '';
      this.browser_uid = obj?.browser_uid || '';
    }
  }


export class userLogin {
  username: string;
  password: string;
  browser_uid: string;

  constructor(obj?:any){
    this.username = obj?.username || '';
    this.password= obj?.password || '';
    this.browser_uid = obj?.browser_uid || '';

  }
}


export class userRegister{
  name:string;
  phone:number;
  email:string;
  password:string;
  tax_code:string;

  constructor(obj?:any){
    this.name = obj?.fullName || '';
    this.phone= obj?.phoneNumber || '';
    this.email = obj?.email || '';
    this.password= obj?.password || '';
    this.tax_code= obj?.companyCode || '';
  }
}


