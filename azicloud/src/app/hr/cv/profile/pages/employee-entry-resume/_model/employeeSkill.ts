
export class EmployeeSkill {
    skill_types: [
        {
            id: number,
            name: string
        }
    ]
    skill_list: [
        {
            id: number,
            skill_type_id: number,
            skill_name: string,
            skill_level_name: string,
            level_process: number
        }
    ]
}

