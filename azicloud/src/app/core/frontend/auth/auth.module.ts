import { NgModule } from "@angular/core";
import { AuthRoutingModule } from "./auth-routing.module";
import { AuthComponent } from "./auth.component";
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginService } from "./_services/login.service";

import { WebReqInterceptor } from "./_services/webreqinterceptor";
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { CommonModule } from "@angular/common";
import { ShareModule } from "./_share/share.module";
import { OptComponent } from './pages/otp/opt.component';

import { VerifyTokenTimeoutComponent } from './pages/verify-token-timeout/verify-token-timeout.component';
import { ForgotPasswordComponent } from "./pages/forgot-password/forgot-password.component";
import { AuthService } from "./_services/auth.service";
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';

import  firebase from 'firebase/app';

import { CountdownModule } from 'ngx-countdown';
import { RegisterWorkPlaceComponent } from './pages/register-work-place/register-work-place.component';
import { SecurityQuestionComponent } from './pages/security-question/security-question.component';
import { LinkComponent } from './pages/link/link.component';
import { ChoosePlaceComponent } from './pages/choose-place/choose-place.component';

import { NzModalModule } from 'ng-zorro-antd/modal';
import { LinkOtpComponent } from './pages/link-otp/link-otp.component';
import { AppSharedModule } from "src/app/_share/appShare.module";
import { CookieService } from 'ngx-cookie-service';

firebase.initializeApp(environment.firebase);

@NgModule({
    declarations:[
       AuthComponent,
       LoginComponent,
       RegisterComponent,
       ForgotPasswordComponent,
       ChangePasswordComponent,
       OptComponent,
       VerifyTokenTimeoutComponent,
       RegisterWorkPlaceComponent,
       SecurityQuestionComponent,
       LinkComponent,
       ChoosePlaceComponent,
       LinkOtpComponent,
    ],
    imports:[
      AppSharedModule,
      AuthRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      ShareModule,
      CommonModule,
      AngularFireAuthModule,
      CountdownModule,
      NzModalModule
    ],
    exports:[],
    providers:[
      LoginService,
      AuthService,
      CookieService
    ]
})

export class AuthModule {}
