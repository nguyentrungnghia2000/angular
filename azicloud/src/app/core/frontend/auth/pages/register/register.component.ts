import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { switchMap, tap } from 'rxjs/operators';
import { AuthService } from '../../_services/auth.service';
import firebase from 'firebase/app';
import 'firebase/auth';
import { environment } from 'src/environments/environment';
import { WindowService } from '../../_services/window.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Register } from '../../_models/register.model';
import { LoginService } from '../../_services/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  signUpForm: FormGroup;
  windowRef: any;
  lick = null
  numberphone:number = 84;
  readonly = false;

  activatedDisplay = false;
  loginPageDisplay = false;

  register: Register;

constructor(private win: WindowService,
    private router: Router,
    private fb: FormBuilder,
    private auth: AuthService,
    private toastr: ToastrService,
    private loginsService: LoginService) {
      this.register= new Register();
      let facebooke_email = localStorage.getItem('facebook_email');
      let google_email = localStorage.getItem('googlee_email');
      if((facebooke_email !== null && facebooke_email !== undefined) || (google_email !== null && google_email !== undefined)){
        if(facebooke_email !== null && facebooke_email !== undefined){
          this.register.email = facebooke_email;
          this.readonly = true;
        } else {
          this.register.email = google_email;
          this.readonly = true;
        }
      }
    }


  ngOnInit(): void {
  }

  public change(event:any):void{
    this.numberphone = event.target.value;
  }

  registerUser() {
    if(this.register.name !== undefined && this.register.phone !== undefined && this.register.email !== undefined && this.register.password !== undefined){
      this.auth.signup(this.register.name, this.register.phone, this.register.email,
        this.register.password, 'form', 'string', 'string').pipe().subscribe((res: any) => {
            if (res.result === 1) {
              localStorage.setItem('user_id', res.id);
              localStorage.setItem('user_name', this.register.email);
              this.toastr.success('Đăng ký thành công. Hãy xác nhận má otp được gửi trong mail của bạn', 'THÔNG BÁO')
              this.router.navigate(['/core/frontend/auth/otp']);
            } else if(res.result === -2) {
              this.toastr.warning('Trùng email','CẢNH BÁO');
            } else if(res.result === -3) {
              this.toastr.warning('Email đã đăng ký nhưng chưa kích hoạt.','CẢNH BÁO');
              this.activatedDisplay = true;
            } else if(res.result === -4) {
              this.toastr.warning('Số điện thoại đã đăng ký nhưng chưa kích hoạt.','CẢNH BÁO');
              this.activatedDisplay = true;
            } else if(res.result === -5) {
              this.toastr.warning('Email đã bị khóa.','LỖI');
            } else if(res.result === -6) {
              this.toastr.warning('Số điện thoại đã bị khóa.','LỖI');
            } else if(res.result === -7) {
              this.toastr.warning('Email đã sử dụng.','CẢNH BÁO');
              localStorage.setItem('register_email', this.register.email);
              this.loginPageDisplay = true;
            } else if(res.result === -8) {
              this.toastr.warning('Số điện thoại đã được sử dụng.','CẢNH BÁO');
              this.loginPageDisplay = true;
            }
        });
    } else {
      this.toastr.error('Vui lòng nhập đầy đủ thông tin để đăng ký', 'LỖI');
    }

  }



  sendCode() {

    const num = localStorage.getItem('user_Info');
    const pNumber = JSON.parse(num);
    const appVerifier = this.windowRef.recaptchaVerifier;
    firebase.auth().signInWithPhoneNumber(pNumber.phoneNumber, appVerifier)
      .then((confirmationResult) => {

        this.windowRef.confirmationResult = confirmationResult;
      }).catch((error) => {
        console.log(error);
      });
  }

  closeDialog(){
    this.activatedDisplay = false;
    this.loginPageDisplay = false;
  }

  moveToActivate(){
    this.loginsService.renewOTP(this.register.email, 'string').subscribe((res: any) => {
      if(res.result === 1){
        this.closeDialog();
        this.router.navigate(['/core/frontend/auth/otp']);
      }
    });
  }

  moveToLoginPage(){
    this.closeDialog();
    this.router.navigate(['/core/frontend/auth/login']);
  }

  showPass = false;
  showEye = false;

  showPassword(){
    this.showPass = !this.showPass;
    this.showEye = !this.showEye;
  }
}
