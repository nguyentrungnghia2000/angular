import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Company } from '../../_models/company.module';

@Component({
  selector: 'app-choose-place',
  templateUrl: './choose-place.component.html',
  styleUrls: ['./choose-place.component.scss']
})
export class ChoosePlaceComponent implements OnInit {

  companyList: Company[] = [];
  company: Company;

  constructor(private router: Router) {
    this.companyList = JSON.parse(localStorage.getItem("listCompany"));
    this.company = new Company();
   }

  ngOnInit(): void {
  }

  onChoose(name: any){
    for(let i = 0; i < this.companyList.length; i++){
      if(this.companyList[i].name === name){
        localStorage.setItem("usercompany", JSON.stringify(this.companyList[i]));
      }
    }
    localStorage.removeItem("listCompany");
    this.router.navigate(['/hr']);
  }

}
