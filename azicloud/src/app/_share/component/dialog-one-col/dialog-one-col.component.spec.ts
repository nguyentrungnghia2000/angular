import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogOneColComponent } from './dialog-one-col.component';

describe('DialogOneColComponent', () => {
  let component: DialogOneColComponent;
  let fixture: ComponentFixture<DialogOneColComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogOneColComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogOneColComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
