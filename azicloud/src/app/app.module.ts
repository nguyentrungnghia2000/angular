
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FacebookLoginProvider, GoogleLoginProvider, SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule, registerLocaleData } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AppleSigninModule } from 'ngx-apple-signin'
import { WebReqInterceptor } from './core/frontend/auth/_services/webreqinterceptor';
import { ToastrModule } from 'ngx-toastr';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import en from '@angular/common/locales/en';
import { LoaderInterceptorService } from './_share/services/loader-interceptor.service';
import { AppSharedModule } from './_share/appShare.module';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    SocialLoginModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    AppleSigninModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    })// ToastrModule added
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass: WebReqInterceptor,
      multi:true
    },
    {
      provide:HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi:true
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(
              '2987909328097263'
            ),

          },
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '715576938476-0lokvujpdnnotv4kij2r0builol6bugl.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    },
    { provide: NZ_I18N, useValue: en_US }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
