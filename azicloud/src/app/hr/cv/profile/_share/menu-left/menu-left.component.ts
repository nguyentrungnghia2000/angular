import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-left',
  templateUrl: './menu-left.component.html',
  styleUrls: ['./menu-left.component.scss']
})
export class MenuLeftComponent implements OnInit {
  info="font-weight: 600;"
  contact="font-weight: 400;"
  resume="font-weight: 400;"
  constructor() { }

  ngOnInit(): void {
  }

  Active(E){
    switch (E) {
      case "info":
        this.info="font-weight: 600;"
        this.contact="font-weight: 400;"
        this.resume="font-weight: 400;"
          break;
      case "contact":
        this.info="font-weight: 400;"
        this.contact="font-weight: 600;"
        this.resume="font-weight: 400;"
          break;
      case "resume":
        this.info="font-weight: 400;"
        this.contact="font-weight: 400;"
        this.resume="font-weight: 600;"
          break;
  }
}
}
