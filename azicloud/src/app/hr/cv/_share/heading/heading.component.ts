import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.scss']
})
export class HeadingComponent implements OnInit {

  items:MenuItem[]
  constructor() { }

  ngOnInit(): void {
    this.items =
      [
        { label: 'Nhân Viên' },
        { label: 'Thông Tin' }
      ]
  }

}
