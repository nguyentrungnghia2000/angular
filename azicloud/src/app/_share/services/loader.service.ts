import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  public isLoading$ = new BehaviorSubject<boolean>(false);
  public readonly loading$ = this.isLoading$.asObservable();

  constructor() { }

  setLoading(isLoading: boolean) {
    this.isLoading$.next(isLoading);
  }

  getLoading(){
    return this.isLoading$;
  }
}
