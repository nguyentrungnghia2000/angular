import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { Bank } from '../../../_models/dropdown/bank';
import { EmployeeBank } from '../../../_models/employee-info-entry/empolyee-bank';
import { InfoEntryService } from '../../../_services/info-entry.service';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {

  display: boolean = false;
  loadData = false;
  employeeBank: EmployeeBank;
  employeeBankUpdate: EmployeeBank;
  userId!: any;
  uid: any;

  ListBanks: Bank[] = [];
  bank: Bank;
  loading$ = this.loaderService.isLoading$;

  constructor(private infoEntryService: InfoEntryService,
              private toastr: ToastrService,
              public loaderService: LoaderService,
              private renderer: Renderer2,
              @Inject(DOCUMENT) private doc: Document) {
    this.employeeBank = new EmployeeBank();
    this.userId = localStorage.getItem('user');
    this.uid = parseInt(this.userId);
   }

  ngOnInit(): void {
    this.getListBanks();
    this.loadEmployeeBank();
  }

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  //show-display dialog
  showDialog() {
    this.employeeBankUpdate = new EmployeeBank();
    this.loadEmployeeBankUpdate();
    this.display = true;
    this.updateBodyPadding();
  }

  closeDialog(){
    this.display = false;
    this.updateBodyPadding(true);
  }

  loadEmployeeBank(){
    this.loadData = true;
    this.infoEntryService.getEmployeeBank(this.uid).subscribe((res: any) =>{
      if(res !== null){
        this.employeeBank = res;
        this.loadData = false;
      } else{
        this.toastr.error('Không thể tải về thông tin ngân hàng', 'LỖI');
      }
    });
  }

  loadEmployeeBankUpdate(){
    this.infoEntryService.getEmployeeBank(this.uid).subscribe((res: any) =>{
      if(res !== null){
        this.employeeBankUpdate = res;
      } else{
        this.toastr.error('Không thể tải về thông tin ngân hàng', 'LỖI');
      }
    });
  }

  //get list banks for combobox
  getListBanks(){
    this.infoEntryService.getListBanks(1).pipe(map((result: any) =>
    result.map((data: any) => {
      return this.ListBanks.push(this.bank = new Bank(data.id, data.name, data.trading_name));
    }))).subscribe((res: any) =>{
      if(res !== null){
      }
      else{
        this.toastr.error('Tải danh sách ngân hàng thất bại', 'LỖI');
      }
    });
  }

  updateEmployeeBank(){

    this.infoEntryService.updateEmployeeBank(this.uid, this.employeeBankUpdate.bank_id,
                                                this.employeeBankUpdate.bank_account_name,
                                                this.employeeBankUpdate.bank_account_number,
                                                this.employeeBankUpdate.bank_branch).subscribe((res: any) => {
      if(res.result === 1){
        this.toastr.success('Cập nhật thông tin ngân hàng thành công', 'THÔNG BÁO');
        this.loadEmployeeBank();
        this.closeDialog();
      } else{
        this.toastr.error('Cập nhật thông tin ngân hàng thất bại', 'LỖI');
      }
    });
  }

  onSelectedBankUpdate(event){
    let changedValue = event;
    for(let i = 0; i < this.ListBanks.length; i++){
      if(this.ListBanks[i].name === changedValue){
        this.employeeBankUpdate.bank_id = this.ListBanks[i].id;
      }
    }
  }

}
