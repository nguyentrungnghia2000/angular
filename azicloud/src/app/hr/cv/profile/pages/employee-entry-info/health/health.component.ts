import { DOCUMENT } from '@angular/common';
import { Parser } from '@angular/compiler/src/ml_parser/parser';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from 'primeng/api';
import { map } from 'rxjs/operators';
import { LoaderService } from 'src/app/_share/services/loader.service';
import { Configs } from '../../../_models/dropdown/config';
import { EmployeeHealth } from '../../../_models/employee-info-entry/employee-health';
import { InfoEntryService } from '../../../_services/info-entry.service';

@Component({
  selector: 'app-health',
  templateUrl: './health.component.html',
  styleUrls: ['./health.component.scss']
})
export class HealthComponent implements OnInit {
  //http://api.azicloud.vn/api/v1/hr/employee/1/employee-health/list

  display: boolean = false;
  updateDisplay: boolean = false;
  deleteDisplay: boolean = false;
  loadData = false;
  checkDate: Date;
  selectedDate: any;
  userId: any;
  uid: any;
  updateDateChecked = false;

  health_id: any = 0;
  employeeHealth: EmployeeHealth;
  employeeHealthUpdate: EmployeeHealth;
  employeeHealths: EmployeeHealth[] = [];
  ListBlood: Configs[] =[];
  blood: Configs;

  loading$ = this.loaderService.isLoading$;

  constructor(private infoEntryService: InfoEntryService,
              private toastr: ToastrService,
              public loaderService: LoaderService,
              private confirmationService: ConfirmationService,
              private renderer: Renderer2,
              @Inject(DOCUMENT) private doc: Document) {
    this.userId = localStorage.getItem('user');
    this.uid = parseInt(this.userId);
    this.employeeHealth = new EmployeeHealth();
    this.getListBloods();
   }

  ngOnInit(): void {
    this.loadEmployeeHealth();
  }

  //hide browser scrollbar
  updateBodyPadding(isReset=false) {
    if (isReset) {
      this.renderer.removeStyle(this.doc.body, 'padding-right');
    } else {
      const diff = window.innerWidth - this.doc.body.clientWidth || 0;
      this.renderer.setStyle(this.doc.body, 'padding-right', diff + 'px');
    }
  }

  // show dialog create
  showDialog() {
    this.display = true;
    this.loadEmployeeHealth();
    this.employeeHealth = new EmployeeHealth();
    this.updateBodyPadding();
  }

  // show update health dialog
  showDialogUpdate() {
    this.updateDisplay = true;
    this.updateBodyPadding();
  }

  displayDialogDelete(health_id:number){
    this.deleteDisplay = true;
    this.health_id = health_id;
    this.updateBodyPadding();
  }

  closeDialog(){
    this.display = false;
    this.updateDisplay = false;
    this.deleteDisplay = false;
    this.updateBodyPadding(true);
  }

  // get id for update employee health button
  dialogUpdate(employeeHealth_id: number){
    this.employeeHealthUpdate = new EmployeeHealth();
    this.infoEntryService.getEmployeeHealth(employeeHealth_id).subscribe((data: any) => {
      if(data !== null){
        this.employeeHealthUpdate = data;
        this.checkDate = new Date(this.employeeHealthUpdate.check_date);
      }
    })
    this.showDialogUpdate();
  }

  //get list bloodgroup
  getListBloods(){
    this.infoEntryService.getListBloods().pipe(map((result: any) =>
    result.map((data: any) => {
      return this.ListBlood.push(this.blood = new Configs(data.value, data.key));
    }))).subscribe((res: any) =>{
      if(res !== null){
      }
    })
  }

  loadEmployeeHealth(){
    this.loadData = true;
    this.infoEntryService.getListEmployeeHealth(this.uid).subscribe((res: any) =>{
      if(res !== null){
        this.employeeHealths = res;
        for(let i = 0; i< this.employeeHealths.length; i++){
          this.employeeHealths[i].check_date = this.getDateOnly(this.employeeHealths[i].check_date);
        }
      } else{
        this.toastr.error('Không thể tải về thông tin sức khỏe', 'LỖI');
      }
      this.loadData = false;
    })
  }

  createEmployeeHealth(){

    if(this.employeeHealth.height !== undefined && this.employeeHealth.weight !== undefined && this.employeeHealth.blood_key !== undefined &&
        this.employeeHealth.status !== undefined && this.employeeHealth.check_date !== undefined &&
        this.employeeHealth.congenital_diseases !== undefined && this.employeeHealth.note !== undefined){

          this.infoEntryService.createEmployeeHealth(this.uid, -1, this.employeeHealth.height, this.employeeHealth.weight,
                                                  this.employeeHealth.blood_key, this.employeeHealth.status, this.employeeHealth.check_date,
                                                  this.employeeHealth.congenital_diseases, this.employeeHealth.note).subscribe((res: any) =>{
            if(res.result === 2){
              this.toastr.success('Cập nhật lần đo sức khỏe thành công', 'THÔNG BÁO');
              this.closeDialog();
            }
            else if(res.result === 1){
              this.toastr.success('Thêm mới lần đo sức khỏe thành công', 'THÔNG BÁO');
              this.closeDialog();
            }
            else{
              this.toastr.error('Thêm mới lần đo sức khỏe thất bại', 'LỖI');
            }
          });
      } else{
        this.toastr.warning('Chưa nhập đầy đủ thông tin. Vui lòng kiểm tra lại', 'CẢNH BÁO');
      }

  }

  deleteEmployeeHealth(){

    let uid = this.health_id
    if(this.health_id !== 0){
      this.infoEntryService.deleteEmployeeHealth(uid).subscribe((res: any) =>{
        if(res.result === 1){
          this.health_id = 0;
          this.loadEmployeeHealth();
          this.closeDialog();
          this.toastr.success('Xóa thành công', 'THÔNG BÁO');
        } else {
          this.toastr.error('Xóa thất bại', 'LỖI');
        }
      });
    } else {
      this.toastr.error('Không tìm thấy health id', 'LỖI');
    }
  }



  updateEmployeeHealth(){

    if(!this.updateDateChecked){
      let d = new Date(Date.parse(this.employeeHealthUpdate.check_date));
      this.employeeHealth.check_date = d.toISOString();
    }
    this.infoEntryService.createEmployeeHealth(this.uid, this.employeeHealthUpdate.id, this.employeeHealthUpdate.height, this.employeeHealthUpdate.weight,
                                              this.employeeHealthUpdate.blood_key, this.employeeHealthUpdate.status, this.employeeHealthUpdate.check_date,
                                              this.employeeHealthUpdate.congenital_diseases, this.employeeHealthUpdate.note).subscribe((res: any) =>{
      if(res.result === 2){
        if(this.updateDateChecked){
          this.loadEmployeeHealth();
          this.updateDateChecked = false;
        }
        this.toastr.success('Cập nhật lần đo sức khỏe thành công', 'THÔNG BÁO');
        this.closeDialog();
      }
      else if(res.result === 1){
        this.loadEmployeeHealth();
        this.toastr.success('Thêm mới lần đo sức khỏe thành công', 'THÔNG BÁO');
        this.closeDialog();
      }
      else{
        this.toastr.error('Cập nhật lần đo sức khỏe thất bại', 'LỖI');
      }
    });
  }

  //convert date to JSON string
  onSelectDateCreate(event) {
    let d = new Date(Date.parse(event));
    this.employeeHealth.check_date = d.toISOString();
  }

  onSelectDateUpdate(event) {
    let d = new Date(Date.parse(event));
    this.employeeHealthUpdate.check_date = d.toISOString();
    this.updateDateChecked = true;
  }

  //split date to show on label
  getDateOnly(str: string){
    let arrTemp: string[] = str.split('T');
    return arrTemp[0];
  }

  //match blood key by blood group
  onChangeHealth(event){
    let changedValue = event;
    for(let i = 0; i < this.ListBlood.length; i++){
      if(this.ListBlood[i].value === changedValue){
        this.employeeHealth.blood_key = this.ListBlood[i].key;
      }
    }
  }

  onChangeHealthUpdate(event){
    let changedValue = event;
    for(let i = 0; i < this.ListBlood.length; i++){
      if(this.ListBlood[i].value === changedValue){
        this.employeeHealthUpdate.blood_key = this.ListBlood[i].key;
      }
    }
  }
}
